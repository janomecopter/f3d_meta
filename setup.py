import sys
import codecs
import os.path
from setuptools import setup

if sys.version_info < (3, 6):
    sys.exit('Python 3.6 is the minimum required version')


def read(rel_path):
    here = os.path.abspath(os.path.dirname(__file__))
    with codecs.open(os.path.join(here, rel_path), 'r') as fp:
        return fp.read()


def get_version(rel_path):
    for line in read(rel_path).splitlines():
        if line.startswith('__version__'):
            delim = '"' if '"' in line else "'"
            return line.split(delim)[1]
    else:
        raise RuntimeError("Unable to find version string.")


INSTALL_REQUIRES = [
    'numpy',
    'pandas',
    'scikit-learn',
    'scikit-image',
    'SFP>=0.0.19',
    'openpyxl',
    'Metashape',
    'mahotas',
    'tables'
]

setup(
    name='f3d_meta',
    version=get_version("f3d_meta/__init__.py"),
    packages=['f3d_meta'],
    url='',
    license='GNU GPLv3',
    author='Bryan Hally',
    author_email='bryan.hally@rmit.edu.au',
    description='Handles and processes for recovering metrics from Fuels3D point clouds',
    py_modules=['f3d_meta'],
    install_requires=INSTALL_REQUIRES
)
