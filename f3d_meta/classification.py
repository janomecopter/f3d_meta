import copy
import mahotas as mh
import numpy as np
import pandas as pd
from skimage.future.graph import RAG
from skimage.measure import regionprops_table
from sklearn.decomposition import PCA
from f3d_meta.segmentation import create_ortho3d_intensity


def lambda_apply_pca(values):
    """
    applies a principal component analysis to work out a implied dimensionality of a set of features,
    then reports the strength of the translated space via eigenvalues
    :param values: the values to be fitted
    :return: the eigenvalues of the resultant PCA fit
    """
    pca = PCA()
    return pca.fit(values).explained_variance_ratio_


def calc_green_diff(values):
    """
    calculate green_diff index
    :param values: an (n x x) x 3 array of RGB values
    :return: an (n x x) array of green_diff values
    """
    return np.mean(values[:, 1] - (values[:, 0] + values[:, 2]) / 2, axis=0)


def calc_green_indx(values):
    """
    calculate green_index values in a handy lambda format
    :param values: an (n x x) x 3 array of RGB values
    :return: an (n x x) array of green_index values
    """
    return np.mean(values[:, 1] / np.sum(values, axis=1), axis=0)


def determine_adjsegs(segs):
    """
    uses skimage.graph.RAG to determine the adjacent segments from a labelled voxel space
    :param segs: labelled numpy array
    :return: dict of identifiers of all segments adjacent to each segment
    """
    seg_graph = RAG(segs)
    neighbours = seg_graph.adj
    # python 3 looooooves generators - ugh; explicitly make everything lists
    nkeys = list(neighbours.keys())
    # so many parentheses
    adjseg = {k: np.array(list((k, *list(neighbours[k].keys())))) for k in nkeys}
    return adjseg


def select_points_by_seglist_and_gb(points_groupby, seglist):
    """
    Takes one or a number of segment ids and return all points within them
    :param points: points DataFrame
    :param seglist: list, or array, of ints
    :return: selected_points
    """
    if not isinstance(seglist, np.ndarray):
        seglist = np.array(seglist)

    # check whic segmetns are in points_groupby
    groupby_keys = np.array(list(points_groupby.groups.keys()))
    # compare with the seglist
    val_segs = np.isin(seglist, groupby_keys)
    # chip out the relevant segments from seglist
    seglist = np.array(seglist)[val_segs]
    # check there are zero points
    if len(seglist) == 0:
        return 0
    else:
        # return the points in each segment of seglist
        return pd.concat((points_groupby.get_group(seg) for seg in seglist))


def create_feature_table(pc):
    """
    handler for when i'm too lazy to write pc.points in my function calls
    :param pc: SFP object containing obj.points, a pd.DataFrame containing point information
    :return: feature table described in create_feature_table_from_points
    """
    return create_feature_table_from_points(pc.points)


def create_feature_table_from_points(points):
    """
    one big function which creates a bunch of feature descriptors on a segment by segment basis
    :param points: a pd.DataFrame object containing point cloud and segmentation information
    :return: feature_space: pd.DataFrame
        elements of feature_space:
        - mean, std dev and median of segment colours in all elements of the LAB and RGB colour spaces
        - the thirteen haralick texture description features for each segment where available (texture is
        calculated in three dimensions, then in two dimensions based on flattening of the feature space
        using a PCA if this is unsuccessful - if texture is subsequently not available, this absence is noted)
        - PCA eigenvalues for segment characteristics in xyz, rgb and lab feature spaces, for each segment
        individually, and for the segment and its adjoining peers
        - green_diff and green_indx indices for each segment
        - the number of points, the xyz ranges of each segment, and the mean height of the segment points above
        the lowest point in the xy coverage of the segment
        - the density of segment points
    """
    # group the points by their supervoxel labels
    grp = points.groupby('supervox_label')
    # starting the feature space by adding colour metrics
    for i, operator in enumerate(['mean', 'std', 'median']):
        fdf = grp.agg({'r': operator, 'g': operator, 'b': operator,
                       'lab_l': operator, 'lab_a': operator, 'lab_b': operator}).rename(
                columns=dict(r=f'rgb_r_{operator}', g=f'rgb_g_{operator}', b=f'rgb_b_{operator}',
                             lab_l=f'lab_l_{operator}', lab_a=f'lab_a_{operator}', lab_b=f'lab_b_{operator}')
            )
        if not i:
            feature_space = fdf
        else:
            feature_space = feature_space.join(fdf)

    # define a PCA
    pca = PCA()

    # create a 3d space and populate with segment labels
    segs = np.zeros((points.z_vox_ind.max() + 1,
                     points.y_vox_ind.max() + 1,
                     points.x_vox_ind.max() + 1), dtype=np.uint32)
    segs[(points.z_vox_ind.values,
          points.y_vox_ind.values,
          points.x_vox_ind.values)] = points.supervox_label
    # determine the adjacent segments to each segment
    adjseg = determine_adjsegs(segs)
    # create a default table using regionprops_table (scikit-image.measure)
    rtdf = pd.DataFrame(regionprops_table(segs))
    rtdf = rtdf.set_index('label')
    # create an intensity orthovoxel space
    ortho = create_ortho3d_intensity(points)

    # texture metrics from voxel_space intensity
    txt_feats = dict()
    txt_error = list()
    for ind in rtdf.index.values:
        # grab the bounding box created in regionprops_table
        bbox = rtdf.loc[ind]
        # copy the ortho space, restrict by the bounding box and divide by 256 (converting from
        # 16-bit to 8-bit values
        oc = copy.deepcopy(ortho)
        oc = oc[bbox[0]:bbox[3], bbox[1]:bbox[4], bbox[2]:bbox[5]] // 256
        # zero out values which have different labels associated with them
        oc[np.where(segs[bbox[0]:bbox[3], bbox[1]:bbox[4], bbox[2]:bbox[5]] != bbox.name)] = 0
        # grab texture metrics where available
        d = get_texture_from_seg(bbox.name, oc.astype(np.uint8))
        # if it returns a dict, use it to update the txt_feats dict
        if isinstance(d, dict):
            txt_feats.update(d)
        else:
            # otherwise it goes in the error pile
            txt_error.append(d)

    # so, the error pile gets flattened in the least important physical dimension, then we try texture again
    for ind in txt_error:
        # flatten to 2d to give texture
        bbox = rtdf.loc[ind]
        g = grp.get_group(ind)
        # work out which axis needs to go, the smallest component of a PCA
        flatten_axis = np.argmin(np.abs(pca.fit(g[['z', 'y', 'x']].values).components_[0]))
        # copy the ortho space, restrict by the bounding box and divide by 256 (converting from
        # 16-bit to 8-bit values
        oc = copy.deepcopy(ortho)
        oc = oc[bbox[0]:bbox[3], bbox[1]:bbox[4], bbox[2]:bbox[5]] // 256
        # zero out values which have different labels associated with them
        oc[np.where(segs[bbox[0]:bbox[3], bbox[1]:bbox[4], bbox[2]:bbox[5]] != bbox.name)] = 0
        # mean the values along flatten_axis
        oc = np.nanmean(np.ma.array(oc, mask=oc == 0), axis=flatten_axis)
        # make the nans zero - redundant? probably, but i don't want to play with it
        oc[np.isnan(oc)] = 0
        # get texture
        d = get_texture_from_seg(bbox.name, oc.data.astype(np.uint8))
        # if successful, update txt_feats
        if isinstance(d, dict):
            txt_feats.update(d)
            # print(f'Segment {list(d.keys())[0]} succeeds')
        else:
            # well, that's a great big downer. still, we can't feed nans to random forests
            # so we'll just have to handle these separately when the time comes
            pass
            # print(f'Segment {d} fails in 2D too')

    # tack those txt_feats into the feature_space
    feature_space = feature_space.join(pd.DataFrame.from_dict(
        txt_feats, orient='index', columns=[f'haralick_{i:02d}' for i in np.arange(13) + 1]))

    rgb_pca_seg = dict()
    xyz_pca_seg = dict()
    lab_pca_seg = dict()
    rgb_pca_l2 = dict()
    xyz_pca_l2 = dict()
    lab_pca_l2 = dict()
    green_diff = dict()
    green_indx = dict()
    point_feats = dict()
    seg_range = dict()

    for g in grp:  # g consists of name, group_items
        # pca of rgb, xyz, lab in the primary segment
        rgb_pca_seg.update({g[0]: pca.fit(g[1][['r', 'g', 'b']].values).explained_variance_ratio_})
        xyz_pca_seg.update({g[0]: pca.fit(g[1][['x', 'y', 'z']].values).explained_variance_ratio_})
        lab_pca_seg.update({g[0]: pca.fit(g[1][['lab_l', 'lab_a', 'lab_b']].values).explained_variance_ratio_})
        # grab the points from adjacent segments as well
        gg = select_points_by_seglist_and_gb(grp, adjseg[g[0]])
        # more pcas
        rgb_pca_l2.update({g[0]: pca.fit(gg[['r', 'g', 'b']].values).explained_variance_ratio_})
        xyz_pca_l2.update({g[0]: pca.fit(gg[['x', 'y', 'z']].values).explained_variance_ratio_})
        lab_pca_l2.update({g[0]: pca.fit(gg[['lab_l', 'lab_a', 'lab_b']].values).explained_variance_ratio_})
        # green indices
        green_diff.update({g[0]: calc_green_diff(g[1][['r', 'g', 'b']].values)})
        green_indx.update({g[0]: calc_green_indx(g[1][['r', 'g', 'b']].values)})
        # segment geometric ranges
        seg_range.update({g[0]: g[1][['z', 'y', 'x']].apply(lambda x: x.max() - x.min()).values})
        # height above lowest point in the vertical column covered by the segment
        centroid_height = g[1].z.mean() - points[np.logical_and.reduce(
            (points.y > g[1].y.min(), points.y < g[1].y.max(),
             points.x > g[1].x.min(), points.x < g[1].x.max()))].z.min()
        # number of points
        group_len = g[1].shape[0]
        point_feats.update({g[0]: [centroid_height, group_len]})

    # bunch of updates, bringing the recently calculated information into the feature table
    feature_space = feature_space.join(pd.DataFrame.from_dict(
        rgb_pca_seg, orient='index', columns=[f'rgb_pca_v{a}_seg' for a in np.arange(3) + 1]))
    feature_space = feature_space.join(pd.DataFrame.from_dict(
        xyz_pca_seg, orient='index', columns=[f'xyz_pca_v{a}_seg' for a in np.arange(3) + 1]))
    feature_space = feature_space.join(pd.DataFrame.from_dict(
        lab_pca_seg, orient='index', columns=[f'lab_pca_v{a}_seg' for a in np.arange(3) + 1]))
    feature_space = feature_space.join(pd.DataFrame.from_dict(
        rgb_pca_l2, orient='index', columns=[f'rgb_pca_v{a}_l2' for a in np.arange(3) + 1]))
    feature_space = feature_space.join(pd.DataFrame.from_dict(
        xyz_pca_l2, orient='index', columns=[f'xyz_pca_v{a}_l2' for a in np.arange(3) + 1]))
    feature_space = feature_space.join(pd.DataFrame.from_dict(
        lab_pca_l2, orient='index', columns=[f'lab_pca_v{a}_l2' for a in np.arange(3) + 1]))
    feature_space = feature_space.join(pd.DataFrame.from_dict(
        green_diff, orient='index', columns=['green_diff']))
    feature_space = feature_space.join(pd.DataFrame.from_dict(
        green_indx, orient='index', columns=['green_indx']))
    feature_space = feature_space.join(pd.DataFrame.from_dict(
        point_feats, orient='index', columns=['centroid_column_height', 'number_points']))
    feature_space = feature_space.join(pd.DataFrame.from_dict(
        seg_range, orient='index', columns=['seg_z_range', 'seg_y_range', 'seg_x_range']))
    # calculate volumes for segment density
    seg_vols = np.product(feature_space[['seg_z_range', 'seg_y_range', 'seg_x_range']].values, axis=1)
    seg_vols[seg_vols == 0] = 1e-9
    # density - points per cubic metre
    feature_space['seg_density'] = feature_space['number_points'] / seg_vols
    return feature_space


def create_features_from_list_of_segments(points, seglist):
    """
    helper function to create truncated feature spaces based upon a supplied seglist
    :param points: a pd.DataFrame object containing point cloud and segmentation information
    :param seglist: a list of segments to be examined from the larger group of points contained in points
    :return: feature_space as defined in create_feature_table
    """
    points = points[points.supervox_label.isin(seglist)]
    return create_feature_table_from_points(points)


def get_texture_from_seg(segnum, oc):
    """
    calculates haralick textural information from a orthovoxel representation of a segment
    :param segnum: the segment identifier (int)
    :param oc: a (z, y, x) array of voxel values
    :return: dict of haralick values per segment (if available), else returns the segment id only
    """
    try:
        texture_feats = mh.features.haralick(oc, ignore_zeros=True, return_mean=True)
    except ValueError:
        # print(segnum)
        return segnum
    return {segnum: texture_feats}


'''
def main_old():
    sample_nums = [f'S{x}' for x in np.arange(9) + 1]

    for i, sample_num in enumerate(sample_nums):
        print(f'Creating features for {sample_num}')
        file_folder = '/mnt/solid_store/data/F3D_segmentation'
        file_name = f'plot 1 - Black Mountain_kp20000_{sample_num}_SFP.pkl'
        with open(os.path.join(file_folder, file_name), 'rb') as f:
            pc = pickle.load(f)

        st = time.time()
        feature_space = create_feature_table(pc)
        ft = time.time()
        print(f'Time taken: {ft - st:.1f} secs')

        xls_file = [f for f in os.listdir('/mnt/solid_store/data/F3D_segmentation')
                    if sample_num in f if 'xlsx' in f][0]
        seg_classes = load_class_info(f'/mnt/solid_store/data/F3D_segmentation/{xls_file}')
        # if sample_num == 'S1':
        #     seg_classes = seg_classes.append(pd.DataFrame.from_dict({1008: 'leaflitter', 1887: 'leaflitter'},
        #                                                             orient='index', columns=['digitised_class']))
        # elif sample_num == 'S8':
        #     seg_classes = seg_classes.append(pd.DataFrame.from_dict({1996: 'woodydebris'},
        #                                                             orient='index', columns=['digitised_class']))
        digi_classes = seg_classes.join(feature_space)

        feature_space['sample'] = sample_num
        feature_space = feature_space.set_index(['sample', feature_space.index])

        digi_classes['sample'] = sample_num
        digi_classes = digi_classes.set_index(['sample', seg_classes.index])

        if not i:
            mfs = feature_space
            digi_segs = digi_classes
        else:
            mfs = pd.concat((mfs, feature_space))
            digi_segs = pd.concat((digi_segs, digi_classes))

    digi_segs = digi_segs[digi_segs.number_points > 10]
    digi_segs = digi_segs.dropna()

    pred_df = train_random_forest(digi_segs, mfs)
    classes = {v: k + 3 for k, v in enumerate(np.unique(pred_df.predicted_class))}
    for i, sample_num in enumerate(sample_nums):
        file_folder = '/mnt/solid_store/data/F3D_segmentation'
        file_name = f'plot 1 - Black Mountain_kp20000_{sample_num}_SFP.pkl'
        with open(os.path.join(file_folder, file_name), 'rb') as f:
            pc = pickle.load(f)

        point_pred = pred_df.loc[sample_num]
        assign_predictions_to_points(pc, point_pred, classes=classes, sample_num=sample_num)
'''

if __name__ == "__main__":
    pass

