import SFP
import numpy as np
from skimage.segmentation import slic
from skimage.color import rgb2lab
import os


def pc_select_by_radius(points, centre, radius):
    """
    Selects points from the point cloud within a defined radius
    :param pc: SFP object
    :param centre: dict containing x, y coords of centroid
    :param radius: radius of area to be covered
    :return: pc object with the relevant points only
    """
    # check that centre is formatted correctly
    if isinstance(centre, (list, tuple, np.ndarray)):
        centre = dict(x=centre[0], y=centre[1])
    # check if centroid is within pc bounds
    bounds = points[['x', 'y']].apply(lambda x: [x.min(), x.max()])
    if not np.logical_and.reduce((centre['x'] > bounds.x[0], centre['x'] < bounds.x[1],
                                  centre['y'] > bounds.y[0], centre['y'] < bounds.y[1])):
        print(f'Selected centre ({centre["x"]}, {centre["y"]}) not within point cloud')
        return 0
    dist = np.array([np.sqrt(np.sum([x**2 for x in item]))
                     for item in points[['x', 'y']].values - np.array([centre['x'], centre['y']])])
    points = points[dist < radius]
    return points


def add_lab_colors_to_points(points):
    """
    adds colours as defined in the CIELAB color space to points in the point cloud
    :param points: pd.Dataframe containing an SFP.points object
    :return: adds to the points object in place
    """
    lab_labels = ['lab_l', 'lab_a', 'lab_b']
    lab = rgb2lab(points[['r', 'g', 'b']].values)
    for i, item in enumerate(np.transpose(lab)):
        points[lab_labels[i]] = item


def lambda_get_median(values):
    """
    lambda function to obtain the value of the point closest to the median of the values given to the function.
    This is used in conjunction with a pd.groupby().apply() to obtain the value with the minimum squared distance
    from the multiple medians in the long axis of values.
    :param values: an n x m array of values
    :return: the value of the point closest to the median of the m variables given
    """
    medians = values.median()
    med_diff = np.sum((values - medians) ** 2, axis=1)
    return values.iloc[med_diff.argmin()]


def create_ortho3d_lab_medians(points):
    """
    this function creates an empty voxel space based upon the dimensions of the voxel indexes in points,
    and then populates the occupied voxels with the lab_medians based upon their location in the voxel space
    :param points: pd.Dataframe containing an SFP.points object
    :return: ortho: an (z,y,x,3) array of aggregated voxel values
    """
    try:
        points.lab_l
    except AttributeError:
        add_lab_colors_to_points(points)
    mdf = points.groupby(['z_vox_ind', 'y_vox_ind', 'x_vox_ind'])
    ortho_df = mdf[['lab_l', 'lab_a', 'lab_b']].apply(lambda x: lambda_get_median(x))
    ortho = np.zeros((points.z_vox_ind.max() + 1,
                      points.y_vox_ind.max() + 1,
                      points.x_vox_ind.max() + 1, 3), dtype=np.uint16)
    ind = [tuple(int(val) for val in vals) for vals in ortho_df.index.values]
    ortho[tuple(np.transpose(ind))] = ortho_df.values
    return ortho


def get_segments_ortho3d(ortho, compactness=1.0, supervox_size=10):
    """
    takes an orthophoto/orthovoxel space and segments it based upon the slic segmentation algorithm
    from scikit-image
    :param ortho: an (z,y,x,3) array of aggregated voxel values
    :param compactness: the compactness factor of supervoxels used by slic
    :param supervox_size: the expected dimensions of one side of an average segment
    :return: labelled segment map of (z, y, x) dims
    """
    # check for multichannel data
    if len(ortho.shape) == 4:
        n_segments = np.product(np.array(ortho[:, :, :, 0].shape) / supervox_size)
        multichannel = True
    else:
        n_segments = np.product(np.array(ortho.shape) / supervox_size)
        multichannel = False
    segs = slic(ortho,
                n_segments=n_segments,
                compactness=compactness,
                multichannel=multichannel,
                start_label=1)
    # np.savez_compressed(pc.file_path[:-4] + '_segs.npz', segs=segs)
    return segs


def create_ortho3d_intensity(points):
    """
    this function creates an empty voxel space based upon the dimensions of the voxel indexes in points,
    and then populates the occupied voxels with the mean intensity based upon their location in the voxel space
    :param points: pd.Dataframe containing an SFP.points object
    :return: ortho: an (z,y,x) array of aggregated voxel values
    """
    mdf = points.groupby(['z_vox_ind', 'y_vox_ind', 'x_vox_ind'])
    ortho_df = mdf.agg({'intensity': 'mean'})
    ortho = np.zeros((points.z_vox_ind.max() + 1,
                      points.y_vox_ind.max() + 1,
                      points.x_vox_ind.max() + 1), dtype=np.uint16)
    ind = [tuple(int(val) for val in vals) for vals in ortho_df.index.values]
    ortho[tuple(np.transpose(ind))] = ortho_df.intensity.values
    return ortho


def assign_supervox_segments(pc, spr_labels):
    """
    assigns labels of the segmented voxel space back onto the points in the point cloud object, along with
    the associated voxel space of the SFP object
    :param pc: a SFP.pointCloud object
    :param spr_labels: the segmented labels derived from get_segments_ortho3d
    :return: the SFP.pointCloud object, with obj.points and obj.voxel_space altered
    """
    indices = (pc.points.z_vox_ind.values, pc.points.y_vox_ind.values, pc.points.x_vox_ind.values)
    pc.points['supervox_label'] = spr_labels[indices]
    indices = (pc.voxel_space.z_vox_ind.values, pc.voxel_space.y_vox_ind.values, pc.voxel_space.x_vox_ind.values)
    pc.voxel_space['supervox_label'] = spr_labels[indices]
    return pc


def randomise_vs_labels(pc):
    """
    indexes and randomises the voxel space labels created using SLIC, to aid in visualisation of the segmentation space
    :param pc: a SFP.pointCloud object
    """
    vs_random_labels = np.arange(len(pc.voxel_space.supervox_label.unique()))
    np.random.shuffle(vs_random_labels)
    pc.voxel_space['label_random'] = -1
    grp = pc.voxel_space.groupby('supervox_label')
    for i, (k, v) in enumerate(grp.groups.items()):
        pc.voxel_space.loc[v, 'label_random'] = vs_random_labels[i]
    grp = pc.points.groupby('supervox_label')
    for i, (k, v) in enumerate(grp.groups.items()):
        pc.points.loc[v, 'label_random'] = vs_random_labels[i]


def segment_main_process(filepath, metadata=None):
    """
    the main processing workflow of segmentation.py
    :param filepath: path to individual LAS file produced by meta_processing.build_clouds()
    :param metadata: project metadata to supply project details such as sitename and plotname
    :return: filepath of the associated SFP.points object saved in pandas.HDF format
    """
    cell_size = 0.005
    pc = SFP.pointCloud(filepath)
    pc.points = pc_select_by_radius(pc.points, (100, 500), 1)
    pc.points = pc.points[pc.points.z < 30.25]
    pc.create_voxel_space(cell_size, agg_method='median')
    add_lab_colors_to_points(pc.points)
    ortho = create_ortho3d_lab_medians(pc.points)
    segs = get_segments_ortho3d(ortho, 0.01, 10)
    pc = assign_supervox_segments(pc, segs)
    randomise_vs_labels(pc)
    # if not os.path.isdir(os.path.join(os.path.split(os.path.dirname(filepath))[0], 'segmentation')):
    #     os.makedirs(os.path.join(os.path.split(os.path.dirname(filepath))[0], 'segmentation'))
    h5_path = os.path.join(os.path.split(os.path.dirname(filepath))[0], 'segmentation')
    if metadata:
        plot_name = f'{"".join(metadata["sitename"].split("_"))}_{"".join(metadata["plotname"].split("_"))}'
    else:
        plot_name = '.'.join(os.path.basename(filepath).split('.')[:-1]).split('_')[0]
    sample_id = '_'.join('.'.join(os.path.basename(filepath).split('.')[:-1]).split('_')[2:])
    pc.points.to_hdf(os.path.join(h5_path, f'{plot_name}.h5'), sample_id)
    return os.path.join(h5_path, f'{plot_name}.h5')

