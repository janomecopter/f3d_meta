import os
import shutil
import argparse
from f3d_meta.meta_processing import import_and_align, build_clouds
from f3d_meta.segmentation import segment_main_process
from f3d_meta.random_forest import get_training_labels, classify_clouds
from f3d_meta.calc_metrics import calc_metrics


def process_clouds_only(plot_path):
    plot_name = os.path.split(plot_path)[1]
    las_files = [f for f in os.listdir(os.path.join(plot_path, 'ms_output')) if '.las' in f]
    for f in las_files:
        print(f'Segmenting {f} commenced')
        segments_filepath = segment_main_process(os.path.join(plot_path, 'ms_output', f))
    training_labels = get_training_labels(training_label_loc)
    print(f'Classifying {plot_path} point clouds')
    prediction_filepath = classify_clouds(segments_filepath, training_labels)
    print(f'Calculating metrics for {plot_path}')
    metrics = calc_metrics(prediction_filepath)
    metrics.to_hdf(os.path.join(plot_path, 'metrics', f'{plot_name}_metrics.h5'), 'df')
    metrics.to_csv(os.path.join(plot_path, 'metrics', f'{plot_name}_metrics.csv'))


# location of training label pandas file
training_label_loc = '/mnt/spinny_store/cloudstor/e76243/Shared/Fuels3D end-user trials' \
                     '/Trial 1/segment_labelling/master_labels.h5'

'''
parser = argparse.ArgumentParser()
parser.add_argument("plot_path")
args = parser.parse_args()

plot_path = args.plot_path
'''


def main(plot_path):
    plot_name = os.path.split(plot_path)[1]
    # get sample folder names
    sample_folders = [f for f in os.listdir(plot_path) if os.path.isdir(os.path.join(plot_path, f))]

    # make folder structure
    # if not os.path.isdir(os.path.join(plot_path, 'images')):
    os.makedirs(os.path.join(plot_path, 'images'))
    # move sample folders to an "images" folder
    for item in sample_folders:
        shutil.copytree(os.path.join(plot_path, item), os.path.join(plot_path, 'images', item))
        shutil.rmtree(os.path.join(plot_path, item))
    os.makedirs(os.path.join(plot_path, 'ms_output'))
    os.makedirs(os.path.join(plot_path, 'segmentation'))
    os.makedirs(os.path.join(plot_path, 'classification'))
    os.makedirs(os.path.join(plot_path, 'metrics'))

    # run alignment process from meta_processing
    meta_proj_name = os.path.join(plot_path, 'images', f'{plot_name}.psx')
    good_samples, bad_samples = import_and_align(plot_path, savepath=meta_proj_name)
    if len(good_samples) < 4:
        print('Your plot has failed')
        # send failed email script
        # delete project files script
    else:
        # delete the sample folders which were not successful
        for sample in bad_samples:
            shutil.rmtree(os.path.join(plot_path, 'images', sample))
        # send ready for control email script
        # move project to accessible bucket for download

    # this is where the manual control target step is done
    # control need to be defined by the user in Metashape according to the defined control scheme detailed in
    # meta_processing.py file

    # build point clouds and save las files
    build_clouds(meta_proj_name)
    process_clouds_only(plot_path)
