import argparse
import json
import os
import platform

from f3d_meta.calc_metrics import calc_metrics
from f3d_meta.meta_processing import build_clouds
from f3d_meta.random_forest import get_training_labels, classify_clouds
from f3d_meta.segmentation import segment_main_process

# location of training label pandas file
if platform.node() == 'smgs-seo-dedede':
    training_label_loc = '/mnt/solid_store/data/F3D_Trials/master_labels.h5'
    # training_label_loc = '/mnt/spinny_store/cloudstor/e76243/Shared/Fuels3D end-user trials' \
    #                      '/Trial 1/segment_labelling/master_labels.h5'
else:
    print('Fix for your machine using platform.node()')
    raise ValueError

parser = argparse.ArgumentParser()
parser.add_argument("plot_path")
args = parser.parse_args()


def process_clouds_only(plot_path):
    with open(os.path.join(plot_path, [f for f in os.listdir(plot_path) if '.json' in f][0])) as f:
        metadata = json.load(f)
    plot_name = f'{"".join(metadata["sitename"].split("_"))}_{"".join(metadata["plotname"].split("_"))}'
    las_files = [f for f in os.listdir(os.path.join(plot_path, 'ms_output')) if '.las' in f]
    for f in las_files:
        print(f'Segmenting {f} commenced')
        segments_filepath = segment_main_process(os.path.join(plot_path, 'ms_output', f), metadata=metadata)
    training_labels = get_training_labels(training_label_loc)
    print(f'Classifying {plot_path} point clouds')
    prediction_filepath = classify_clouds(segments_filepath, training_labels, metadata=metadata)
    print(f'Calculating metrics for {plot_path}')
    # TODO: extract landscape type from json metadata, to alter the metrics calculated
    metrics = calc_metrics(prediction_filepath, metadata=metadata)
    # metrics = calc_metrics(prediction_filepath, landscape_type)
    # TODO: set csv floating point precision
    metrics.to_hdf(os.path.join(plot_path, 'metrics', f'{plot_name}_metrics.h5'), 'df')
    metrics.to_csv(os.path.join(plot_path, 'metrics', f'{plot_name}_metrics.csv'))
    # TODO: grab metrics from csv/hdf for reporting


if __name__ == "__main__":
    plot_path = args.plot_path
    os.makedirs(os.path.join(plot_path, 'ms_output'))
    os.makedirs(os.path.join(plot_path, 'segmentation'))
    os.makedirs(os.path.join(plot_path, 'classification'))
    os.makedirs(os.path.join(plot_path, 'metrics'))
    # build point clouds and save las files
    proj_name = os.path.join(plot_path, 'images',
                             [f for f in os.listdir(os.path.join(plot_path, 'images')) if '.psx' in f][0])
    build_return = build_clouds(proj_name)
    process_clouds_only(plot_path)
