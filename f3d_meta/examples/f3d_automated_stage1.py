import os
import json
import shutil
import argparse
from f3d_meta.meta_processing import import_and_align


parser = argparse.ArgumentParser()
parser.add_argument("plot_path")
args = parser.parse_args()


def main(plot_path):
    """
    This process sets up the project folder structure for further processing, then runs a function
    for photo matching and alignment. Samples are identified as good or bad, and a threshold is used
    to determine whether to continue with processing
    :param plot_path: path to sample images
    :return: status, pass or fail
    """
    with open(os.path.join(plot_path, [f for f in os.listdir(plot_path) if '.json' in f][0])) as f:
        metadata = json.load(f)
    plot_name = f'{"".join(metadata["sitename"].split("_"))}_{"".join(metadata["plotname"].split("_"))}'
    # get sample folder names
    sample_folders = [f for f in os.listdir(plot_path) if os.path.isdir(os.path.join(plot_path, f))]

    # make folder structure
    # if not os.path.isdir(os.path.join(plot_path, 'images')):
    os.makedirs(os.path.join(plot_path, 'images'))
    # move sample folders to an "images" folder, get rid of underscores!!!
    for item in sample_folders:
        shutil.copytree(os.path.join(plot_path, item),
                        os.path.join(plot_path, 'images', "".join(item.split("_"))))
        shutil.rmtree(os.path.join(plot_path, item))

    # run alignment process from meta_processing
    meta_proj_name = os.path.join(plot_path, 'images', f'{plot_name}.psx')
    good_samples, bad_samples = import_and_align(plot_path, savepath=meta_proj_name)

    if len(good_samples) < 4:
        print('Your plot has failed')
        # send failed email script
        # delete project files script
        return 1
    else:
        # delete the sample folders which were not successful
        print('Your plot has been successful')
        for sample in bad_samples:
            shutil.rmtree(os.path.join(plot_path, 'images', sample))
        # send ready for control email script
        # move project to accessible bucket for download
        return 0


if __name__ == "__main__":
    plot_path = args.plot_path
    return_status = main(plot_path)