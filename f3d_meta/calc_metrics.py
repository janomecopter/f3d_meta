import pandas as pd
import numpy as np
import SFP
# if SFP.__version__ < '0.0.19':
#     raise EnvironmentError('SFP needs to be 0.0.19 or greater')

# fpath = '/mnt/solid_store/data/F3D_Trials/ACT Parks/plot 1 - Black Mountain/classification/predicted_classes.h5'

# assumptions that these are sensible ground filter parameters for all plots - these may need revision
csf_params = {'cloth_resolution': 0.03, 'class_threshold': 0.015,
              'iterations': 1000, 'rigidness': 3, 'time_step': 0.5}

# classes will remain split by these criteria
surface_fuel = ['leaflitter']
elevated_fuel = ['grass', 'forb']
elevated_non_fuel = ['woodydebris', 'controltarget', 'tree']
surface = ['bareearth', 'rock']
other = ['noise', 'mixed']

# cell size remains 5 mm
cell_size = 0.005
# create a circular mask to determine the area of the sample
a = np.arange(2 / cell_size) * cell_size - 1
x, y = np.meshgrid(a, a)
circ_mask = np.sqrt(x ** 2 + y ** 2) <= 1


def calc_metrics(fpath, metadata=None):
    """
    This function takes in a pandas HDF5 file containing point clouds on a per sample basis,
    and returns a series of metrics based upon commonly used assessment metrics for landscape classes
    commonly covered in the fuel assessment paradigm.
    :param fpath: str
        file path to pandas HDF file containing point cloud info
    :param metadata: dict
        metadata information, including sitename and plotname
    :return: metrics: pd.Dataframe
        Pandas dataframe containing calculated metrics on a per sample basis
    """
    # read points in
    d = SFP.read_hdf(fpath)
    # create a separate dataframe to populate with results
    metrics = pd.DataFrame(index=d.keys())
    metrics.index.name = 'sample_id'
    for key in d.keys():
        spc = d[key]
        # generate a grid for populating the dtm
        spc.generate_grid(resolution=cell_size)
        # filter to determine ground points (for determining agh for near-surface); 3 denotes CSF process
        spc.apply_ground_filter(csf_params, 3)
        # make a dtm based on the ground points
        spc.generate_dtm_grid()
        # normalise creates a obj.agh column containing above ground heights
        spc.normalise_points()
        # assume forb, grass in elevated_fuel
        a = spc.points[spc.points.predicted_class.isin(elevated_fuel)]
        # elevated fuel height per segment mean
        metrics.loc[key, f'elevated_fuel_height_mean_seg_(m)'] = a[a.agh > 0].groupby(['x_vox_ind', 'y_vox_ind'])[
            'agh'].mean().mean()
        # elevated fuel height per segment staandard deviation
        metrics.loc[key, f'elevated_fuel_height_std_seg_(m)'] = a[a.agh > 0].groupby(['x_vox_ind', 'y_vox_ind'])[
            'agh'].mean().std()
        # 90th and 10th percentile heights of all near-surface classes
        metrics.loc[key, f'elevated_90th_percentile_height_(m)'] = a[a.agh > 0].agh.quantile(0.9)
        metrics.loc[key, f'elevated_10th_percentile_height_(m)'] = a[a.agh > 0].agh.quantile(0.1)
        # percentage cover for all near-surface segments as a portion of the surveyed sample area
        metrics.loc[key, f'elevated_fuel_cover_portion_(\\%)'] = len(
            a.groupby(['x_vox_ind', 'y_vox_ind']).groups) / np.count_nonzero(circ_mask)
        # surface fuel is pretty much just leaflitter in this analysis
        a = spc.points[spc.points.predicted_class.isin(surface_fuel)]
        # percentage cover for all surface segments as a portion of the surveyed sample area
        metrics.loc[key, f'surface_fuel_cover_portion_(\\%)'] = len(
            a.groupby(['x_vox_ind', 'y_vox_ind']).groups) / np.count_nonzero(circ_mask)
        # biased by sloped surfaces relative to the scaling target
        # also not sure how informative this metric is, the minimum height is 5mm
        metrics.loc[key, f'surface_fuel_thickness_(m)'] = len(a.groupby(['x_vox_ind', 'y_vox_ind', 'z_vox_ind']).groups) /\
            len(a.groupby(['x_vox_ind', 'y_vox_ind']).groups) * cell_size
        # no volume due to potential propagation of error in the voxel space

    plot_name = f'{"".join(metadata["sitename"].split("_"))}_{"".join(metadata["plotname"].split("_"))}'
    metrics['plot_name'] = plot_name
    metrics.reset_index(inplace=True)
    metrics.set_index(['plot_name', 'sample_id'])
    return metrics

# for element in elevated_fuel:
#     a = spc.points[spc.points.predicted_class == element]
#     metrics.loc[key, f'{element}_height_mean'] = a[a.agh > 0].groupby('supervox_label')['agh'].mean().mean()
#     metrics.loc[key, f'{element}_height_std'] = a[a.agh > 0].groupby('supervox_label')['agh'].mean().std()
#     metrics.loc[key, f'{element}_cover'] = len(
#         a.groupby(['x_vox_ind', 'y_vox_ind']).groups) / np.count_nonzero(circ_mask)
# for element in surface_fuel:
#     a = spc.points[spc.points.predicted_class == element]
#     # TODO: perform a PCA on each segment and determine thickness
#     metrics.loc[key, f'{element}_cover'] = len(
#         a.groupby(['x_vox_ind', 'y_vox_ind']).groups) / np.count_nonzero(circ_mask)
#     # biased by sloped surfaces relative to the scaling target
#     metrics.loc[key, f'{element}_thickness'] = len(a.groupby(['x_vox_ind', 'y_vox_ind', 'z_vox_ind']).groups) / \
#         len(a.groupby(['x_vox_ind', 'y_vox_ind']).groups) * cell_size