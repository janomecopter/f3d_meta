import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from f3d_meta.classification import create_feature_table_from_points
import os
import SFP


# training_label_loc = '/mnt/spinny_store/cloudstor/e76243/Shared/Fuels3D end-user trials' \
#                      '/Trial 1/segment_labelling/master_labels.h5'


def get_training_labels(loc):
    """
    function for grabbing training labels
    :param loc: file path to pandas.HDF5 file containing training segment features
    :return: training labels, pd.Dataframe containing all training segment attributes
    """
    # read the hdf file
    training_labels = pd.read_hdf(loc, 'df')
    for col in training_labels.columns:
        # rummage through the data collector (dc) cols, make the nans into none
        if col.startswith('dc'):
            training_labels.loc[training_labels[col].isna(), col] = 'none'
    # nullify segments with ten points or less
    training_labels = training_labels[training_labels.number_points > 10]
    # cols = [col for col in training_labels.columns if 'v3' not in col]
    # cols = [col for col in cols if 'number_points' not in col]
    return training_labels


def segments_points_to_las(points, outfile_path, label='supervox_label'):
    """
    function for outputting the points in each sample to las format. The supervoxel label is stashed
    in intensity, and the class in the classification attribute
    :param points: points: a pd.DataFrame object containing point cloud and segmentation information
    :param outfile_path: path to location of save, filename included
    :param label: which label gets stored in intensity; can be either 'supervox_label' or 'label_random'
    """
    header = SFP.laspy.header.Header(point_format=2)
    # Open our output file
    out_file = SFP.laspy.file.File(outfile_path, mode="w", header=header)

    out_file.header.offset = [points['x'].min(), points['y'].min(), points['z'].min()]
    out_file.header.scale = [0.001, 0.001, 0.001]

    out_file.X = (points['x'] - points['x'].min()) / 0.001
    out_file.Y = (points['y'] - points['y'].min()) / 0.001
    out_file.Z = (points['z'] - points['z'].min()) / 0.001
    out_file.Red = points.r
    out_file.Green = points.g
    out_file.Blue = points.b
    out_file.intensity = points[label]
    out_file.classification = points['class']
    out_file.close()


def run_rf(x, y):
    """
    runs a default random forest classifier
    :param x: feature information by element
    :param y: feature classes by element
    :return: classifier, a scikit-learn random forest classifier object
    """
    rf = RandomForestClassifier(n_estimators=1000, n_jobs=4, min_samples_split=5, bootstrap=True, oob_score=True)
    classifier = rf.fit(x, y)
    return classifier


# feature flags based upon texture or lack thereof
features_used = ['all', 'no_texture']
# operator ids
operators = ['OP1', 'OP2', 'both']


def get_classifier(feature_set, operator, training_labels):
    """
    runs a random forest classifier on specific sections of the training database
    :param feature_set: specify whether fitting occurs on the full set of features, or the non-texture ones
        values used: 'all', 'no_texture'
    :param operator: grabs the training data obtained by the relevant operators
        values used: 'OP1', 'OP2', 'both'
    :param training_labels: a pandas.Dataframe containing the training dataset
    :return: classifier, a scikit-learn random forest classifier object
    """
    if feature_set == 'all':
        # drops nan values (will generally only be segments without textural info)
        training_set = training_labels.dropna()
    else:
        # drop the columns pertaining to haralick features
        training_set = training_labels.loc[:, [col for col in training_labels.columns if 'haralick' not in col]]

    if operator is not 'both':
        # run on the set specified by operator
        ts = training_set[training_set[f'dc_{operator}'] != 'none']
        classifier = run_rf(ts.iloc[:, 2:], ts[f'dc_{operator}'])
        op = operator
    else:
        # we will use only the training data which has labels that both operators agree upon
        ts = training_set[training_set.dc_OP1.eq(training_set.dc_OP2)]
        classifier = run_rf(ts.iloc[:, 2:], ts.dc_OP1)
        op = 'OP1'
    # prints some information about the classifier as it's made
    print(f'Operator: {operator}\tFeature Set: {feature_set}\nNumber of Segments: {ts.shape[0]}\n'
          f'OOB Score: {classifier.oob_score_:.3f}\n'
          f'Most Important Feature: {ts.iloc[:, 2:].columns[np.argmax(classifier.feature_importances_)]}\n'
          f'Classes and Counts: {np.unique(ts[f"dc_{op}"], return_counts=True)}')
    return classifier


def classify_clouds(hdf_path, training_labels, metadata=None):
    """
    function that uses defined classifications to classify point clouds
    :param hdf_path: path to pandas HDF file containing point cloud and segment info
    :param training_labels: training labels, pd.Dataframe containing all training segment attributes
    :param metadata: metadata: project metadata to supply project details such as sitename and plotname
    :return: the path location of the 'predicted_classes.h5' file produced by the function
    """
    # get classifier for all features
    class_all = get_classifier('all', 'both', training_labels)
    # get classifier for all but textural features
    class_no_txt = get_classifier('no_texture', 'both',  training_labels)
    # check the sample keys in the HDF file
    with pd.HDFStore(hdf_path) as a:
        samples = a.keys()

    for sample in samples:
        print(f'Processing sample {sample}...')
        # read in the data from hdf_path for sample
        df = pd.read_hdf(hdf_path, sample)
        # create featues based on this data
        ft = create_feature_table_from_points(df)
        # toss away segments with less than 20 pts, these are noise
        noise_segs = ft[ft.number_points <= 20]
        # make a dataframe for the noise and populate it
        noise = pd.DataFrame(index=noise_segs.index, data='noise', columns=['predicted_class'])
        noise['features_used'] = 'not_assessed'
        # subselect the feature table ignoring that noise
        short_ft = ft[ft.number_points > 20]
        # predict classes based on all available features
        txt_classes = class_all.predict(short_ft.dropna())
        # make a dataframe and populate it
        txt_pred = pd.DataFrame(index=short_ft.dropna().index, data=txt_classes, columns=['predicted_class'])
        txt_pred['features_used'] = 'texture'
        # subselect columns that are not haralick, and repeat using the non-haralick classifier
        no_ft = short_ft.loc[:, [col for col in short_ft.columns if 'haralick' not in col]]
        nt_classes = class_no_txt.predict(no_ft.dropna())
        nt_pred = pd.DataFrame(index=no_ft.dropna().index, data=nt_classes, columns=['predicted_class'])
        nt_pred['features_used'] = 'non_texture'
        # toss away segment predictions that already exist with all features
        nt_pred = nt_pred.drop(index=txt_pred.index)
        # concat all information together
        pred_df = pd.concat((txt_pred, nt_pred, noise))
        unassigned = ft.drop(index=pred_df.index)
        # if there's any segments left, we make a separate table for those
        if len(unassigned):
            unass_segs = pd.DataFrame(index=unassigned.index,
                                      data=np.tile(['noise', 'none'], unassigned.shape[0]).reshape(-1, 2),
                                      columns=['predicted_class', 'features_used'])
            pred_df = pd.concat((pred_df, unass_segs))
        # create some columns with default values
        df['predicted_class'] = 'none'
        df['features_used'] = 'none'
        for item in pred_df.index:
            # assign classes to each segment based upon supervox_label
            df.loc[df.supervox_label == item, 'predicted_class'] = pred_df.at[item, 'predicted_class']
            df.loc[df.supervox_label == item, 'features_used'] = pred_df.at[item, 'features_used']
        classes = {v: k + 3 for k, v in enumerate(['bareearth', 'controltarget', 'forb', 'grass', 'leaflitter',
                                                   'mixed', 'noise', 'rock', 'tree', 'woodydebris'])}
        # assign class values (int) based upon the classes dictionary
        for k, v in classes.items():
            df.loc[df.predicted_class == k, 'class'] = v
        # pump out las files
        segments_points_to_las(df, os.path.join(os.path.split(os.path.dirname(hdf_path))[0], 'classification',
                                                f'{sample[1:]}_classified.las'))
        # save the predictions in a separate HDF file
        df.to_hdf(
            os.path.join(os.path.split(os.path.dirname(hdf_path))[0], 'classification', 'predicted_classes.h5'),
            sample[1:], complevel=5)
    # returns path of the predictions hdf file
    return os.path.join(os.path.split(os.path.dirname(hdf_path))[0], 'classification', 'predicted_classes.h5')


