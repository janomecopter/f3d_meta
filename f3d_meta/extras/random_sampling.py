import os
import shutil
import numpy as np


def bc_sampling():
    plot_names = ['C01', 'T20', 'T24']
    for plot_name in plot_names:
        in_folder = f'/mnt/solid_store/data/british_columbia/prelim_data/{plot_name}'
        out_folder = f'/mnt/spinny_store/cloudstor/e76243/BC_Segmentation/sample photos/{plot_name}'
        photo_list = np.random.permutation(os.listdir(in_folder))[:80]
        for photo in photo_list:
            shutil.copy(os.path.join(in_folder, photo), os.path.join(out_folder, photo))


def f3d_sampling(in_folder, out_folder):
    photo_folders = [os.path.join(in_folder, f) for f in os.listdir(in_folder) if 'S' in f[0]]
    for photo_folder in photo_folders:
        photo_list = np.random.permutation(os.listdir(os.path.join(in_folder, photo_folder)))[:6]
        for photo in photo_list:
            if not os.path.isdir(os.path.join(out_folder, os.path.basename(photo_folder))):
                os.makedirs(os.path.join(out_folder, os.path.basename(photo_folder)))
            shutil.copy(os.path.join(photo_folder, photo),
                        os.path.join(out_folder, os.path.basename(photo_folder), photo))
