import vaex
import laspy
import matplotlib.pyplot as plt
import os
import numpy as np


class PointCloud(object):
    """
    A pale imitation of SFP.pointCloud
    """
    def __init__(self, file_path):
        self.file_path = file_path
        self.file_type = os.path.split(file_path)[1].split('.')[-1]
        self.vox_z_resolution = None
        self.vox_xy_resolution = None
        self.points = self.read_point_cloud()

    def read_point_cloud(self):
        pc_file = laspy.file.File(self.file_path, mode='r')
        points = vaex.from_items(*[[x, getattr(pc_file, x)] for x in ['x', 'y', 'z', 'red', 'green', 'blue',
                                                                      'classification']])
        points['point_number'] = np.arange(points.count())
        pc_file.close()
        return points

    def reset_points(self):
        """
        Allows user to reset the points dataframe to its original state.
        """
        self.points = self.read_point_cloud()
        print('Points dataframe for {} reset'.format(os.path.basename(self.file_path)))

    def create_voxel_space(self, xy_resolution, z_resolution=None, lower_left=None, height_field='z',
                           exclude_class=None, include_noise=False, generate_count=False, inplace=True):

        if z_resolution is None:
            z_resolution = xy_resolution
        self.points['x_vox_ind'] = -1
        self.points['y_vox_ind'] = -1
        self.points['z_vox_ind'] = -1
        self.points['vox_count'] = 0

        if exclude_class is not None:
            # TODO: fix this so we can exclude multiple classes
            self.points_inuse = self.points['classification' != exclude_class]
        else:
            self.points_inuse = self.points
        # Specify the vertical dimension
        z = self.points_inuse[height_field].values
        lower_left = [self.points['x'].min() - xy_resolution, self.points['y'].min() - xy_resolution,
                      z.min() - z_resolution]

        self.points_inuse['x_vox_ind'] = np.floor((self.points_inuse.x.values - lower_left[0]) /
                                                  xy_resolution).astype(np.int16)
        self.points_inuse['y_vox_ind'] = np.floor((self.points_inuse.y.values - lower_left[1]) /
                                                  xy_resolution).astype(np.int16)
        self.points_inuse['z_vox_ind'] = np.floor((z - lower_left[2]) / z_resolution).astype(np.int16)

        points_voxel_space = self.points_inuse[np.logical_and.reduce(
            self.points_inuse.x_vox_ind >= 0, self.points_inuse.y_vox_ind >= 0, self.points_inuse.z_vox_ind >= 0
        )]

        voxel_space = points_voxel_space.groupby(['x_vox_ind', 'y_vox_ind', 'z_vox_ind']) \
            .agg({'x': 'size', 'intensity': 'mean', 'red': 'mean', 'green': 'mean', 'blue': 'mean', 'z': 'mean',
                  'classification': 'max'}) \
            .rename(columns={'x': 'vox_count', 'intensity': 'intensity_mean', 'r': 'r_mean', 'g': 'g_mean',
                             'b': 'b_mean', 'z': 'z_mean', 'class': 'class_max'})


