import fuels3D
import os
from f3d_meta.extras import FuelLayerTools as lt
from plot_layout_tools import *
import RasterTools as rt
import numpy as np
import matplotlib.pyplot as plt
import PlottingTools
from shapely.geometry import Polygon

layer_bottoms = np.array([0.1, 0.5, 2])  # top be replaced with llayers from Phils work
layer_names = ['Surface', 'Near Surface', 'Elevated', 'Canopy']

dataBasePath = '/home/sfm-samurai/Pictures/Mallee'
outfolders = ['Height_figs', 'ASCII_grids', 'PointClouds', 'Orthoimages', 'Profiles']
vegType = 'Hummock'
ageClass = 'Intermediate'
tile_buffer = 2
offset = [-0, -0]
raster_res = 0.01

csf_params = {'cloth_resolution': 0.03, 'class_threshold': 0.015,
              'iterations': 1000, 'rigidness': 3, 'time_step': 0.3}

#Lowan Mallee Mature Params
if np.logical_and(vegType =='Lowan', ageClass=='Mature'):
    csf_params = {'cloth_resolution': 0.03, 'class_threshold': 0.015,
                 'iterations': 1000, 'rigidness': 3, 'time_step': 0.20}
elif np.logical_and(vegType =='Hummock', ageClass=='Mature'):
    csf_params = {'cloth_resolution': 0.03, 'class_threshold': 0.015,
                  'iterations': 1000, 'rigidness': 3, 'time_step': 0.3}



#Setup directory
if not os.path.isdir('{}/{}/{}/Final_products/Terrestrial_SfM/'.format(dataBasePath,vegType,ageClass)):
    os.makedirs('{}/{}/{}/Final_products/Terrestrial_SfM/'.format(dataBasePath,vegType,ageClass))
for f in outfolders:
    if not os.path.isdir('{}/{}/{}/Final_products/Terrestrial_SfM/{}'.format(dataBasePath,vegType,ageClass,f)):
        os.makedirs('{}/{}/{}/Final_products/Terrestrial_SfM/{}'.format(dataBasePath,vegType,ageClass,f))

# Get the our sample areas
csv_directory = '{}/{}/{}/Layout/{}_{}_SquarePlotCorners.csv'.format(dataBasePath,vegType,ageClass,vegType,ageClass[0])
plotCorners = pandas.read_csv(csv_directory)

Tiles = plotCorners.groupby('SampleTile').min()

cover = np.zeros([len(layer_names)+1, Tiles.shape[0]])
height_mean = np.zeros([len(layer_names)+1, Tiles.shape[0]])
height_std = np.zeros([len(layer_names)+1, Tiles.shape[0]])

#if True:
#    iTile =0
for iTile, tile in enumerate(Tiles.index):
    tile = Tiles.index[iTile]
    print(tile)
    #Get our point cloud and create our feature space
    this_plot = plotCorners.loc[plotCorners.SampleTile == tile,:]
    p = np.stack((this_plot[['E']].values, this_plot[['N']].values), axis=1)[:, :, 0]
    min_coords = this_plot[['E', 'N']].min().values
    max_coords = this_plot[['E', 'N']].max().values

    f3D = fuels3D.Fuels3dSample('{}/{}/{}/Processed_data/Terrestrial_SfM/PointClouds/Sample{}.las'.format(dataBasePath,vegType,ageClass,tile))
    f3D.extract_sample_area_polygon(p, 1.5)
    f3D.points = f3D.points.loc[f3D.points.use_index,:].copy().reset_index(drop=True)

    #Get rid of some unwanted figures
    f3D.points.loc[:,'x'] = f3D.points['x'] - min_coords[0]
    f3D.points.loc[:,'y'] = f3D.points['y'] - min_coords[1]

    #now ground filter
    f3D.apply_ground_filter(3, csf_params)

    min_coords = this_plot[['E', 'N']].min().values
    max_coords = this_plot[['E', 'N']].max().values

    #Ok now create some grids
    f3D.grid_def = rt.generate_grid_definition(0.02,[0, 0],grid_dims_m=max_coords - min_coords)
    f3D.generate_grid(f3D.grid_def)
    f3D.generateDTMGrid(f3D.grid_def)
    f3D.normalise_points()

    ind = f3D.points.agh <= -0
    f3D.points.loc[ind,'agh'] = np.nan

    #Classify our point clouds
    f3D.create_voxel_space(0.07, 0.1, height_field='agh', exclude_class=2)
    layer_voxels = lt.doRasterPoor(f3D.voxel_space, 0)
    f3D = lt.applyLayerRules(f3D, layer_voxels, layer_bottoms)
    f3D.points = f3D.points.loc[f3D.points.agh >= 0, :].copy()

    #Ok now lets clip to grid and use full coords
    #min_coords = this_plot[['E','N']].min().values
    f3D.points['x'] = f3D.points['x'] + min_coords[0]
    f3D.points['y'] = f3D.points['y'] + min_coords[1]
    f3D.grid_def = rt.generate_grid_definition(raster_res,min_coords,grid_dims_m=max_coords-min_coords)

    f3D.extract_sample_area_polygon(p)
    area = Polygon(p).area

    points = f3D.points.loc[f3D.points.use_index, :].copy()
    points = points.reset_index(drop=True)

    #First lets grid out all fuels
    f3D.points = points.loc[points['class'] > 2,:].copy().reset_index(drop=True)
    f3D.generate_grid(f3D.grid_def, height_field='agh')
    fig, axes = f3D.show_grid('surface',  title='All Fuels')
    plt.clim(0,5)
    plt.savefig('{}/{}/{}/Final_products/Terrestrial_SfM/Height_figs/{}_{}_height.png'.format(dataBasePath,vegType,ageClass,tile, 'AllFuels'))
    plt.close('all')

    cover[len(layer_names),iTile] = (np.sum(~np.isnan(f3D.grid['grid']))*raster_res**2)/area*100
    height_mean[len(layer_names), iTile] = np.nanmean(f3D.grid['grid'])
    height_std[len(layer_names), iTile] = np.nanstd(f3D.grid['grid'])

    # Generate Grids and metrics
    for iClass, layer_name in enumerate(layer_names):
        f3D.points = points.loc[points['class'] == iClass + 3,:].copy().reset_index(drop=True)
        f3D.generate_grid(f3D.grid_def, height_field='agh')
        fig, axes = f3D.show_grid('surface', title=layer_name)

        if layer_bottoms.shape[0] > iClass:
            plt.clim(0, layer_bottoms[iClass])
        else:
            plt.clim(0, 5)
        plt.savefig('{}/{}/{}/Final_products/Terrestrial_SfM/{}/{}_{}_height.png'.format(dataBasePath, vegType,
                                                                                                      ageClass, outfolders[0],tile,
                                                                                                  layer_name))
        cover[iClass, iTile] = (np.sum(~np.isnan(f3D.grid['grid']))*raster_res**2)/area*100
        height_mean[iClass, iTile] = np.nanmean(f3D.grid['grid'])
        height_std[iClass, iTile] = np.nanstd(f3D.grid['grid'])
        f3D.grid_to_ascii('{}/{}/{}/Final_products/Terrestrial_SfM/{}/{}_{}_height.asc'.format(dataBasePath, vegType,
                                                                                                      ageClass, outfolders[1],tile,
                                                                                                  layer_name))
        #plt.close('all')

    f3D.points = points
    f3D.generate_grid(f3D.grid_def)
    f3D.points['grid_index'] = rt.points_xy_to_grid_index(f3D.points, f3D.grid_def)
    f3D.generate_orthophoto()

    f3D.write_las_file('classifiedPoints_{}.las'.format(tile),
                       '{}/{}/{}/Final_products/Terrestrial_SfM/{}/'.format(dataBasePath, vegType, ageClass,outfolders[2]))
    #f3D.export_grids('{}/{}/{}/Final_products/Terrestrial_SfM/{}/classifiedPoints_{}.gif'.format(dataBasePath,vegType,ageClass,outfolders[3],tile), stack_vegheight=True)
    f3D.write_las_file('normalisedPoints_{}.las'.format(tile),
                       '{}/{}/{}/Final_products/Terrestrial_SfM/{}/'.format(dataBasePath, vegType, ageClass,outfolders[2]),norm_height=True)

    f3D = fuels3D.Fuels3dSample( '{}/{}/{}/Final_products/Terrestrial_SfM/{}/normalisedPoints_{}.las'.format(dataBasePath, vegType, ageClass,outfolders[2], 'TA01'))
    f3D.points['agh'] = f3D.points.z
    points = f3D.points.loc[:, ['x', 'y', 'agh', 'class']].copy()
    ax1, ax2 = PlottingTools.height_and_hexbin_plot(points, hexres=0.01, histres=0.05, axis1='x', axis2='agh', axis3='y', ax3range=None, ax2max=None)


    plt.savefig('{}/{}/{}/Final_products/Terrestrial_SfM/{}/{}_{}_profile_fuelground.png'.format(dataBasePath, vegType,
                                                                                                      ageClass, outfolders[4],tile,
                                                                                                  layer_name))

    f3D.points.use_index = np.logical_and(f3D.points.use_index, f3D.points['class'] > 2)
    points = f3D.points.loc[f3D.points.use_index, ['x', 'y', 'agh', 'class']].copy()
    ax1, ax2 = PlottingTools.height_and_hexbin_plot(points, hexres=0.01, histres=0.1, axis1='x', axis2='agh', axis3='y',
                                         ax3range=None, ax2max=None)
    ax1.set_xlim([0, 20])
    plt.savefig('{}/{}/{}/Final_products/Terrestrial_SfM/{}/{}_{}_profile_fuelonly.png'.format(dataBasePath, vegType,
                                                                                                      ageClass, outfolders[4],tile,
                                                                                                  layer_name))
    #plt.close('all')

np.savez('{}/{}/{}/Final_products/Terrestrial_SfM/metrics_height_cover.npz'.format(dataBasePath,vegType,ageClass),
         cover=cover, height_mean=height_mean, height_std=height_std, layer_bottoms=layer_bottoms,
         layer_names=layer_names, Tiles=Tiles)