import SFP
import os
import numpy as np
from f3d_meta.segmentation import segment_main_process, lambda_get_median
from skimage.color import rgb2lab


def create_ortho3d_rgb_medians(points):
    mdf = points.groupby(['z_vox_ind', 'y_vox_ind', 'x_vox_ind'])
    ortho_df = mdf[['r', 'g', 'b']].apply(lambda x: lambda_get_median(x))
    ortho = np.zeros((points.z_vox_ind.max() + 1,
                      points.y_vox_ind.max() + 1,
                      points.x_vox_ind.max() + 1, 3), dtype=np.uint16)
    ind = [tuple(int(val) for val in vals) for vals in ortho_df.index.values]
    ortho[tuple(np.transpose(ind))] = ortho_df.values // 256
    return ortho


def convert_rgb2lab(ortho):
    return rgb2lab(ortho)


def save_segs(pc, segments):
    np.savez_compressed(pc.file_path[:-4] + '_segs.npz', segs=segments)


def assign_supervox_segments_to_voxspace(vs, spr_labels):
    indices = (vs.z_vox_ind.values, vs.y_vox_ind.values, vs.x_vox_ind.values)
    vs['supervox_label'] = spr_labels[indices]
    return vs


def segments_pc_to_las(pc, outfile_path, label='supervox_label'):
    header = SFP.laspy.header.Header(point_format=2)
    # Open our output file
    out_file = SFP.laspy.file.File(outfile_path, mode="w", header=header)

    out_file.header.offset = [pc.points['x'].min(), pc.points['y'].min(), pc.points['z'].min()]
    out_file.header.scale = [0.001, 0.001, 0.001]

    out_file.X = (pc.points['x'] - pc.points['x'].min()) / 0.001
    out_file.Y = (pc.points['y'] - pc.points['y'].min()) / 0.001
    out_file.Z = (pc.points['z'] - pc.points['z'].min()) / 0.001
    out_file.Red = pc.points.r
    out_file.Green = pc.points.g
    out_file.Blue = pc.points.b
    out_file.intensity = pc.points[label]
    out_file.close()


def ortho3d_to_las(pc, outfile_path, label='supervox_label', cell_size=0.005):
    header = SFP.laspy.header.Header(point_format=2)
    # Open our output file
    out_file = SFP.laspy.file.File(outfile_path, mode="w", header=header)
    out_file.header.offset = [np.floor(pc.points['x'].min() / cell_size) * cell_size,
                              np.floor(pc.points['y'].min() / cell_size) * cell_size,
                              np.floor(pc.points['z'].min() / cell_size) * cell_size]
    out_file.header.scale = [cell_size, cell_size, cell_size]
    out_file.X = pc.voxel_space.x_vox_ind
    out_file.Y = pc.voxel_space.y_vox_ind
    out_file.Z = pc.voxel_space.z_vox_ind
    out_file.Red = pc.voxel_space.r_median
    out_file.Green = pc.voxel_space.g_median
    out_file.Blue = pc.voxel_space.b_median
    out_file.intensity = pc.voxel_space[label]
    out_file.close()


def seg_df_to_las(df, outfile_path, label='supervox_label'):
    header = SFP.laspy.header.Header(point_format=2)
    # Open our output file
    out_file = SFP.laspy.file.File(outfile_path, mode="w", header=header)

    out_file.header.offset = [df['x'].min(), df['y'].min(), df['z'].min()]
    out_file.header.scale = [0.001, 0.001, 0.001]

    out_file.X = (df['x'] - df['x'].min()) / 0.001
    out_file.Y = (df['y'] - df['y'].min()) / 0.001
    out_file.Z = (df['z'] - df['z'].min()) / 0.001
    out_file.Red = df.r
    out_file.Green = df.g
    out_file.Blue = df.b
    out_file.intensity = df[label]
    out_file.close()


if __name__ == "__main__":
    projlist = ['Site 3 - Mineshaft Reserve', 'Site 4 - Beveridge Road', 'Site 5 - YNCR 2']
    for proj in projlist:
        folder_path = f'/mnt/solid_store/data/F3D_Trials/CFA/{proj}/ms_output'
        las_files = [os.path.join(folder_path, f) for f in os.listdir(os.path.join(folder_path))
                     if '.las' in f]
        for lasfile in las_files:
            segment_main_process(lasfile)
            print(f'{lasfile} complete')
    '''
    label = 'supervox_label'
    if random:
        randomise_vs_labels(pc)
        label = 'label_random'
    segments_pc_to_las(pc, pc.file_path[:-4] + '_labelled.las', label=label)
    '''