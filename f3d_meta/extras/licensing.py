import Metashape
import os

mod_loc = os.path.dirname(os.path.abspath(__file__))
one_folder_up = os.path.split(mod_loc)[0]


def activate():
    # with open(os.path.join(one_folder_up, 'metashape_license_code.txt')) as f:
    #     metashape_code = f.readline()
    # Metashape.License().activate(metashape_code[:29])
    pass


def deactivate():
    Metashape.License().deactivate()
    [os.remove(os.path.join(one_folder_up, f)) for f in os.listdir(one_folder_up) if '.lic' in f]


def check_active():
    return Metashape.License().valid
