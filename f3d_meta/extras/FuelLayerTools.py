import numpy as np
from scipy import ndimage
from skimage.morphology import watershed, extrema


def create_layer_segments(layer_grid, start_number):
    # Skipping step of opening and closing

    # Def create tree segments
    segments = ndimage.label(~np.isnan(layer_grid))[0].astype(np.float)

    segments[segments == 0] = np.nan
    segments = segments + start_number - 1

    return segments

def pour_top_to_bottom(top_segments, grid_bot, start_number):
    distance = ndimage.distance_transform_edt(np.isnan(top_segments))
    #forest.grid['count'] = poured_segments
    #fig, ax = forest.show_grid('count')
    #local_maxima[local_maxima == 1] = top_segments[local_maxima == 1]
    temp_grid = top_segments.copy()
    temp_grid[np.isnan(temp_grid)] = 0
    poured_segments = watershed(distance, temp_grid, mask=grid_bot).astype(np.float)
    poured_segments[np.logical_and(np.isnan(grid_bot), ~np.isnan(top_segments))] = np.nan
    poured_segments[poured_segments==0]=np.nan

    grid_bot[~np.isnan(poured_segments)] = np.nan
    if np.any(~np.isnan(grid_bot)):
        new_segments = create_layer_segments(grid_bot,start_number)
        new_segments[new_segments == 0] = np.nan
        poured_segments[np.isnan(poured_segments)] = new_segments[np.isnan(poured_segments)]

    return poured_segments

def open_and_close_grid(curr_grid):
    # Define structuring elements
    mid_open = ndimage.generate_binary_structure(2, 1)
    mid_close = np.ones((6, 6))
    mid_close[(0, 5, 5, 0), (0, 5, 0, 5)] = 0

    top_close = np.ones((7, 7))
    top_close[(0, 6, 6, 0, 0, 1, 6, 5, 1, 0, 5, 6), (0, 6, 0, 6, 1, 0, 1, 0, 6, 5, 6, 5)] = 0
    top_open = np.ones((2, 2))

    bot_close = np.ones((5, 5))
    bot_close[(0, 4, 4, 0), (0, 4, 0, 4)] = 0
    bot_open = np.ones((4, 4))
    bot_open[(0, 3, 3, 0), (0, 3, 0, 3)] = 0

    curr_grid[np.isnan(curr_grid)] = 0
    ind = curr_grid > 0
    points = curr_grid[ind]
    top_thresh = np.percentile(points, 80)
    mid_thresh = np.percentile(points, 20)

    ind = curr_grid >= top_thresh
    next_grid_top = ndimage.binary_opening(ndimage.binary_closing(ind, top_close), top_open)
    ind = np.logical_and(curr_grid < top_thresh, curr_grid >= mid_thresh)
    next_grid_mid = ndimage.binary_opening(ndimage.binary_closing(ind, mid_close), mid_open)
    ind = np.logical_and(curr_grid < mid_thresh, curr_grid > 0)
    next_grid_bot = ndimage.binary_opening(ndimage.binary_closing(ind, bot_close), bot_open)
    next_grid = np.logical_or(np.logical_or(next_grid_top,next_grid_mid),next_grid_bot).astype(np.float)

    next_grid[next_grid == 0] = np.nan
    return next_grid

def doRasterPoor(vector_voxel_space, nearSurfaceCutOff=2):
    layers = np.zeros(vector_voxel_space[['x_vox_ind', 'y_vox_ind','z_vox_ind']].max().values+1, dtype=float)

    voxel_space = np.zeros(vector_voxel_space[['x_vox_ind', 'y_vox_ind', 'z_vox_ind']].max().values + 1, dtype=int)
    ind = np.logical_and.reduce((vector_voxel_space['x_vox_ind'].values >= 0, vector_voxel_space['y_vox_ind'].values >= 0,vector_voxel_space['z_vox_ind'].values >=0 ))
    voxel_space[vector_voxel_space.loc[ind,'x_vox_ind'].values, vector_voxel_space.loc[ind,'y_vox_ind'].values, vector_voxel_space.loc[ind,'z_vox_ind'].values] = vector_voxel_space.loc[ind,'vox_count']

    #Construct our top grid
    i_top= vector_voxel_space[['z_vox_ind']].max().values[0]
    top_grid = voxel_space[:,:,i_top]
    top_b_grid = (top_grid > 0).astype(np.float)
    top_b_grid = ndimage.morphology.binary_dilation(top_b_grid).astype(np.float)
    top_b_grid[top_b_grid == 0] = np.nan
    top_s_grid = create_layer_segments(top_b_grid, 1)
    top_s_grid[~np.isnan(top_s_grid)] = i_top
    layers[:,:,i_top] = top_s_grid
    if np.any(~np.isnan(layers[:,:,i_top])):
        topfound = True
        print('top found at layer {}'.format(i_top))
    else:
        topfound = False

    for i in np.arange(i_top - 1, nearSurfaceCutOff, -1, dtype='int16'):
        if topfound:
            next_grid = voxel_space[:,:,i]
            next_b_grid = (next_grid > 0).astype(np.float)
            next_b_grid = ndimage.morphology.binary_dilation(next_b_grid).astype(np.float)
            next_b_grid[next_b_grid == 0] = np.nan #open_and_close_grid(next_grid)
            next_s_grid = pour_top_to_bottom(layers[:,:,i+1], next_b_grid,-1000)


            next_s_grid[next_s_grid < i+1] = i


            layers[:, :, i] = next_s_grid
        else:
            top_grid = voxel_space[:, :, i]
            top_b_grid = (top_grid > 0).astype(np.float)
            top_b_grid = ndimage.morphology.binary_dilation(top_b_grid).astype(np.float)
            top_b_grid[top_b_grid == 0] = np.nan
            top_s_grid = create_layer_segments(top_b_grid, 1)
            top_s_grid[~np.isnan(top_s_grid)] = i
            layers[:, :, i] = top_s_grid
            if np.any(~np.isnan(layers[:, :, i])):
                topfound = True
                print('top found at layer {}'.format(i))
            else:
                topfound = False

    #layers[:,:,0:nearSurfaceCutOff] = 0
    return layers

def applyLayerRules(f3D, layer_voxels, layer_bottoms, start_class=3):
    fuellayers = np.zeros(f3D.voxel_space[['x_vox_ind', 'y_vox_ind', 'z_vox_ind']].max().values + 1, dtype=int) - 1

    layer_voxels[np.isnan(layer_voxels)] = layer_bottoms[0] / f3D.voxel_dims['z_resolution']
    layer_voxels = layer_voxels.astype(np.int32)

    fuellayers[layer_voxels < layer_bottoms[-1] / f3D.voxel_dims['z_resolution']] = start_class
    for i, value in enumerate(np.arange(0,layer_bottoms.size)):
        fuellayers[layer_voxels >= (layer_bottoms[value] / f3D.voxel_dims['z_resolution'])] = i + start_class + 1

    ind = np.logical_and(f3D.points['x_vox_ind'] >= 0, f3D.points['class'] != 2)
    f3D.points.loc[ind,'class'] = fuellayers[f3D.points.loc[ind,'x_vox_ind'].values, f3D.points.loc[ind,'y_vox_ind'].values, f3D.points.loc[ind,'z_vox_ind'].values]

    return f3D