import Metashape
import os
import pandas
import shutil
from f3d_meta.meta_processing import import_and_align, build_clouds


def set_pathing(plot_path):
    """
    sets up folder structure for processing - copies the sample folders into raw and working folders,
    deletes them from the plot_path folder and provides a processing folder for outputs
    :param plot_path: folder containing samples to process
    :return: plot_path
    """
    if not os.path.basename(plot_path) == 'working':
        contents = os.listdir(plot_path)

        dnames = ['raw', 'working', 'ms_output']
        for dname in dnames:
            try:
                os.makedirs(os.path.join(plot_path, dname))
            except FileExistsError:
                pass
        for item in contents:
            [shutil.copytree(os.path.join(plot_path, item), os.path.join(plot_path, dname, item))
             for dname in dnames[:2]]
            shutil.rmtree(os.path.join(plot_path, item))
        return os.path.join(plot_path, 'working')
    else:
        return plot_path


def count_aligned(metashape_file, save_df=True):
    plot_id = os.path.split(metashape_file)[1].split('_')[0]
    keypoints = int(os.path.split(metashape_file)[1].split('p')[-2].split('.')[0])
    doc = Metashape.Document()
    doc.open(metashape_file)
    df = pandas.DataFrame(columns=['PlotID', 'KeypointLimit', 'ChunkID',
                                   'CamerasAligned', 'TotalCameras', 'Tiepoints'])
    for i, chunk in enumerate(doc.chunks):
        num_cameras = len(chunk.cameras)
        num_aligned = 0
        for camera in chunk.cameras:
            if camera.transform:
                num_aligned += 1
        try:
            tie_points = len(chunk.point_cloud.points)
        except TypeError:
            tie_points = 0
        df.loc[i] = [plot_id, keypoints, chunk.label, num_aligned, num_cameras, tie_points]
    if save_df:
        df.to_csv(metashape_file[:-3] + 'csv')
    else:
        return df


def count_aligned_folder(working_dir):
    mdf = []
    for i, metashape_file in enumerate([os.path.join(working_dir, f) for f in os.listdir(working_dir) if '.psx' in f]):
        df = count_aligned(metashape_file, save_df=False)
        if not i:
            mdf = df
        else:
            mdf = pandas.concat((mdf, df), ignore_index=True)
    df_name = working_dir.split(os.sep)[-2] + '.csv'
    mdf.to_csv(os.path.join(working_dir, df_name))


def keypoint_iterator(working_dir):
    keypoint_list = [1000, 2000, 5000, 10000, 15000, 20000]
    for kps in keypoint_list:
        import_and_align(working_dir, keypoint_limit=kps)


def open_project(proj_path):
    app = Metashape.Application()
    app.cpu_enable = True
    doc = Metashape.Document()
    doc.open(proj_path)
    return doc


def disable_chunks(proj_path, cutoff_pc=0.7):
    doc = open_project(proj_path)
    for chunk in doc.chunks:
        num_cameras = len(chunk.cameras)
        num_aligned = 0
        for camera in chunk.cameras:
            if camera.transform:
                num_aligned += 1
        if num_aligned / num_cameras < cutoff_pc:
            chunk.enabled = False
        doc.save(proj_path)


if __name__ == "__main__":
    working_dir = '/mnt/solid_store/data/F3D_Trials/CFA/YNCR/working'
    import_align = True
    if import_align:
        import_and_align(working_dir)
    else:
        build_clouds(os.path.join(working_dir, [f for f in os.listdir(working_dir) if '.psx' in f][0]))