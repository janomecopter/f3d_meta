import os
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from f3d_meta.segment_training.training_library import spreadsheet_comparison
from f3d_meta.classification import create_feature_table_from_points


def train_random_forest(digi_classes, feature_space):
    # drop segments with num_points < 10 and assign to noise
    noise_segs = feature_space[feature_space.number_points <= 10]
    noise = pd.DataFrame(index=noise_segs.index, data='noise', columns=['predicted_class'])
    noise['features_used'] = 'not_assessed'

    valid_segs = feature_space[feature_space.number_points > 10]

    x_txt = digi_classes.iloc[:, 1:]
    y = digi_classes.digitised_class

    txt_clf = RandomForestClassifier(n_estimators=1000, n_jobs=4, min_samples_split=5, bootstrap=True, oob_score=True)
    txt_classifier = txt_clf.fit(x_txt, y)
    print(f'OOB (texture): {txt_classifier.oob_score_}')
    print(f'Most important feature: {x_txt.columns[np.argmax(txt_classifier.feature_importances_)]}')

    # split features based on texture metrics
    feature_texture_space = valid_segs.dropna()
    txt_classes = txt_clf.predict(feature_texture_space)
    txt_pred = pd.DataFrame(index=feature_texture_space.index, data=txt_classes, columns=['predicted_class'])
    txt_pred['features_used'] = 'texture'

    # separate training for non texture classes
    features_not_texture = valid_segs.loc[:, [col for col in feature_space.columns if 'haralick' not in col]]
    features_not_texture = features_not_texture.dropna()
    x_nt = x_txt.loc[:, [col for col in x_txt.columns if 'haralick' not in col]]

    nt_clf = RandomForestClassifier(n_estimators=1000, n_jobs=4, min_samples_split=5, bootstrap=True, oob_score=True)
    nt_classifier = nt_clf.fit(x_nt, y)
    print(f'OOB (non_texture): {nt_classifier.oob_score_}')
    print(f'Most important feature: {x_nt.columns[np.argmax(nt_classifier.feature_importances_)]}')

    nt_classes = nt_clf.predict(features_not_texture)
    nt_pred = pd.DataFrame(index=features_not_texture.index, data=nt_classes, columns=['predicted_class'])
    nt_pred['features_used'] = 'non_texture'
    nt_pred = nt_pred.drop(index=txt_pred.index)

    pred_df = pd.concat((txt_pred, nt_pred, noise))

    # cleanup segs dropped in the non_texture metrics
    unassigned = feature_space.drop(index=pred_df.index)
    unass_segs = pd.DataFrame(index=unassigned.index,
                              data=np.tile(['noise', 'none'], unassigned.shape[0]).reshape(-1, 2),
                              columns=['predicted_class', 'features_used'])
    pred_df = pd.concat((pred_df, unass_segs))
    return pred_df


def assign_predictions_to_points(pc, pred_df, classes, sample_num='S1'):
    pc.points['predicted_class'] = 'none'
    pc.points['features_used'] = 'none'

    for item in pred_df.index:
        pc.points.loc[pc.points.supervox_label == item, 'predicted_class'] = pred_df.at[item, 'predicted_class']
        pc.points.loc[pc.points.supervox_label == item, 'features_used'] = pred_df.at[item, 'features_used']

    for k, v in classes.items():
        pc.points.loc[pc.points.predicted_class == k, 'class'] = v

    pc.points_to_lasfile(pc.points, f'{sample_num}_classified.las', '/mnt/solid_store/data/F3D_segmentation/')


def grab_plot_ft(plot_path):
    label_df = spreadsheet_comparison(os.path.join(plot_path, 'completed_spreadsheets'))
    for i, sample_id in enumerate(set(label_df.index.get_level_values(0))):
        hdf = pd.read_hdf(os.path.join(plot_path, [f for f in os.listdir(plot_path) if '.h5' in f][0]), sample_id)
        ft = create_feature_table_from_points(hdf)
        ft.reset_index(inplace=True)
        ft = ft.rename(columns=dict(level_1='seg_num'))
        ft['sample_id'] = sample_id
        ft = ft.set_index(['sample_id', 'seg_num'])
        # ft = ft.drop(columns=['sample_id', 'seg_num'])
        if not i:
            mft = ft
        else:
            mft = pd.concat((mft, ft))
    return mft, label_df