import os
import shutil
import numpy as np
import pandas as pd
from f3d_meta.classification import create_feature_table_from_points
from f3d_meta.segment_training.excel_driver import load_class_info, make_sheets


def prepare_for_segment_labelling(segmented_pc_path):
    """
    This function creates a number of spreadsheets to be stored in a 'training' folder, which can subsequently be
    used in conjunction with the matlab process to label point cloud segments
    :param segmented_pc_path: path to pandas HDF5 file containing segmented pointcloud info
    """
    training_folder = os.path.join(os.path.dirname(os.path.split(segmented_pc_path)[0]), 'training')
    if not os.path.isdir(training_folder):
        os.makedirs(training_folder)
    segment_training_path = os.path.join(training_folder, os.path.split(segmented_pc_path)[1])
    shutil.copy(segmented_pc_path, segment_training_path)
    make_sheets(segment_training_path)
    print(f'Spreadsheets and segment information can be found in {os.path.split(segment_training_path)[0]}\n'
          f'Run f3d_labels_latest.m in Matlab to visualise segments for labelling purposes')


def ingest_labels_from_spreadsheet(training_data_location, operator_id):
    """
    This function grabs the classification information contained in each of the spreadsheets at the specified
    location, creates the associated feature tables and indexes in preparation for addition to the main
    training dataset
    :param training_data_location: location of segmented point cloud .h5 file and completed spreadsheets
    :param operator_id: id of operator labelling the spreadsheets (OP1, OP2 only!)
    :return: filepath of pandas hdf5 file containing segment feature table and expected classes
    """
    # check that operator is defined correctly, throw error if not
    if operator_id not in ['OP1', 'OP2']:
        raise ValueError('Operator should be either "OP1" or "OP2".')
    # locate the digitised classes spreadsheets
    flist = [os.path.join(training_data_location, f) for f in os.listdir(training_data_location) if f.endswith('.xlsx')]
    plot_id = flist[0].split('_')[0]
    for i, fpath in enumerate(flist):
        fname = os.path.split(fpath)[1]
        # code is sensitive to excess underscores - limit using them in sample and plot names
        _, sample_id = '.'.join(fname.split('.')[:-1]).split('_')
        print('If code errors here, check that you have defined classes in the spreadsheet.')
        # load digitised classes from the spreadsheet
        df = load_class_info(fpath)
        # renames the column based upon operator
        df = df.rename(columns=dict(digitised_class=f'dc_{operator_id}'))
        # labels based on metadata info
        df['sample_id'] = sample_id
        df['plot_id'] = plot_id
        # reset_index is magic
        df.reset_index(inplace=True)
        # rename the newly created index column, and reset the index with plot and sample data
        df = df.rename(columns=dict(index='seg_num'))
        df = df.set_index(['plot_id', 'sample_id', 'seg_num'])
        # concatenate data if not the first time through
        if not i:
            mdf = df
        else:
            mdf = pd.concat([mdf, df])
    # cereate savepath and save data
    savepath = os.path.join(training_data_location, f'{plot_id}_{operator_id}_tlables.h5')
    mdf.to_hdf(savepath, 'df')
    print(f"Data saved in {savepath}")


def merge_operator_labels(first, second):
    """
    function to merge two sets of digitised labels together
    :param first: path to first dataframe
    :param second: path to second dataframe
    :return: path to merged dataframe
    """
    a = pd.read_hdf(first, 'df')
    b = pd.read_hdf(second, 'df')
    # check that both dataframes have "dc_" columns
    if not np.logical_and(len([col for col in a.columns if 'dc_' in a]),
                          len([col for col in b.columns if 'dc_' in a])):
        print('No digitised class column in the dataframes to be merged')
        raise ValueError
    # check that dataframes have unique 'dc_OPx' identifiers
    assert np.all(
        [col for col in a.columns if 'dc_' in col] != [col for col in b.columns if 'dc_' in col]
    ), 'Digitised class columns need unique identifiers'
    c = a.join(b)
    savepath = os.path.join(os.path.split(first)[0], f'{os.path.split(first)[1].split("_")[0]}_merged_tlabels.h5')
    c.to_hdf(savepath, 'df')
    print(f"Merged segments saved at {savepath}")


def add_features_to_operator_labels(class_df_path, segmented_pc_path):
    """
    this function seperates out the feature information for each of the training segments and fleshes out the table
    :param class_df_path: path to pd hdf5 file containing one plot's worth of digitised segments (classes)
    :param seg_file: path to hdf5 file containing all segments of the point cloud for the plot
    """
    class_df = pd.read_hdf(class_df_path, 'df')
    # check that segments come from one plot, else raise error
    plot_id = class_df.index.get_level_values(0).unique()
    if len(plot_id) != 1:
        print('More than one plot included in the digitised_classes file')
        raise ValueError
    # separate the sample_ids and add feature information per segment
    sample_ids = class_df.index.get_level_values(1).unique()
    for i, sample_id in enumerate(sample_ids):
        ddf = pd.read_hdf(segmented_pc_path, sample_id)
        # groupby segment labels, intensive process so best done once
        gdf = ddf.groupby('supervox_label')
        for seg_num in class_df.index.get_level_values(2):
            # runs create_feature_table on points from one segment - vastly improves process time
            ft = create_feature_table_from_points(gdf.get_group(seg_num))
            # rename the index to make joining dataframes easier
            ft.index.rename('seg_num', inplace=True)
            # merge dataframes
            class_df = class_df.merge(ft, how='left', on='seg_num')
        df.reset_index(inplace=True)
        df = df.set_index(['plot_id', 'sample_id', 'seg_num'])
        if not i:
            mdf = df
        else:
            mdf = pd.concat([mdf, df])
    savepath = os.path.join(os.path.split(class_df_path)[0],
                            f'{os.path.split(class_df_path)[1].split("_")[0]}_merged_w_feats.h5')
    mdf.to_hdf(savepath, 'df')
    print(f"Features added to segments saved at {savepath}")


def add_data_to_training_set(training_data_loc, data_to_add_loc):
    """
    function to add segs to the training library - always back up the existing training library before proceeding
    :param training_data_loc: location of existing training library
    :param data_to_add_loc: location of new segment info to add
    """
    ans = input('Always backup before proceeding with this process.\n'
                'Training data will be appended to. Are you sure? (y/N)').lower()
    if ans == 'y':
        training_labels = pd.read_hdf(training_data_loc, 'df')
        appended_labels = pd.read_hdf(data_to_add_loc, 'df')
        # check if columns in appended_labels are labelled correctly
        dc_cols = [col for col in appended_labels.columns if 'dc_' in col]
        if not np.all(dc_cols == ['dc_OP1', 'dc_OP2']):
            print('Digitised class columns are not formatted properly.\nOperators should be labelled dc_OP1, dc_OP2')
            raise ValueError
        # check if appended data is populated
        try:
            if np.isnan(appended_labels.iloc[:, 2].mean()):
                print('There are invalid values in the appended dataset - check and try again.')
                raise ValueError
        # error if no feature columns exist
        except KeyError:
            print('Feature columns not present - make sure to add features before appending.')
            raise ValueError
        # concatenates if no issues found
        training_labels = pd.concat([training_labels, appended_labels])
        # save that file
        training_labels.to_hdf(training_data_loc, 'df')
        print(f'Training labels at {training_data_loc} successfully updated.')
    else:
        print('"y" not entered, no update applied.')


def spreadsheet_comparison(fpath):
    sample_list = list(set([f[:2] for f in os.listdir(fpath)]))
    for i, sample in enumerate(sample_list):
        comparison_sheets_list = [f for f in os.listdir(fpath) if sample in f]
        if len(comparison_sheets_list) == 1:
            continue
        for j, f in enumerate(comparison_sheets_list):
            annotater_id = f.split('-')[1].split('.')[0]
            df = load_class_info(os.path.join(fpath, f))
            df = df.rename(columns=dict(digitised_class=f'dc_{annotater_id}'))
            if not j:
                mdf = df
            else:
                mdf = mdf.join(df)
        mdf.reset_index(inplace=True)
        mdf = mdf.rename(columns=dict(index='seg_num'))
        mdf['sample_id'] = sample
        mdf = mdf.set_index(['sample_id', 'seg_num'])
        if not i:
            jdf = mdf
        else:
            jdf = pd.concat((jdf, mdf))
    return jdf


def create_conf_df(df):
    conf_df = pd.DataFrame(index=np.unique(df.dc_JT), columns=np.unique(df.dc_JT))
    for name, grp in df.groupby('dc_BH'):
        unique_vals = np.unique(grp.dc_JT.values, return_counts=True)
        for i, val in enumerate(unique_vals[0]):
            conf_df.loc[name, val] = unique_vals[1][i]
    return conf_df


def load_all_from_labelling_spreadsheets(fol_loc, plot_id):
    flist = [os.path.join(fol_loc, f) for f in os.listdir(fol_loc) if f.endswith('.xlsx')]
    if len(flist) == 0:
        return 0
    sample_list = list(set([f[:2] for f in os.listdir(fol_loc)]))

    for i, sample in enumerate(sample_list):
        comparison_sheets_list = [f for f in os.listdir(fol_loc) if sample in f]
        if len(comparison_sheets_list) == 1:
            f = comparison_sheets_list[0]
            sample_id, annotater_id = f.split('.')[0].split('-')
            df = load_class_info(os.path.join(fol_loc, f))
            df = df.rename(columns=dict(digitised_class=f'dc_{annotater_id}'))
            df['sample_id'] = sample_id
            df['plot_id'] = plot_id
            df.reset_index(inplace=True)
            df = df.rename(columns=dict(index='seg_num'))
            df = df.set_index(['plot_id', 'sample_id', 'seg_num'])
            if not i:
                mdf = df
            else:
                mdf = pd.concat([mdf, df])
        else:
            for j, f in enumerate(comparison_sheets_list):
                sample_id, annotater_id = f.split('.')[0].split('-')
                df = load_class_info(os.path.join(fol_loc, f))
                df = df.rename(columns=dict(digitised_class=f'dc_{annotater_id}'))
                df['sample_id'] = sample_id
                df['plot_id'] = plot_id
                df.reset_index(inplace=True)
                df = df.rename(columns=dict(index='seg_num'))
                df = df.set_index(['plot_id', 'sample_id', 'seg_num'])
                if not j:
                    sdf = df
                else:
                    sdf = sdf.join(df)
            if not i:
                mdf = sdf
            else:
                mdf = pd.concat([mdf, sdf])

    return mdf


def separate_seg_data(folder):
    plot_id = os.path.basename(folder)
    mdf = load_all_from_labelling_spreadsheets(os.path.join(folder, 'completed_spreadsheets'), plot_id)
    if isinstance(mdf, int):
        return 0, 0
    points_file = os.path.join(folder, [f for f in os.listdir(folder) if f.endswith('.h5')][0])
    temp_mdf = mdf.reset_index()
    for i, (name, grp) in enumerate(temp_mdf.groupby(['plot_id', 'sample_id'])):
        points = pd.read_hdf(points_file, name[1])
        points.loc[:, ['plot_id', 'sample_id']] = name
        if not i:
            mp = points
        else:
            mp = pd.concat([mp, points])
    return mp, mdf


def prep_training_library(mft, label_df):
    # bang feature information on the training segments
    label_df = label_df.join(mft)
    # eliminate training segments below 10 points
    noise_labs = label_df[label_df.number_points <= 10]
    label_noise = pd.DataFrame(index=noise_labs.index, data='noise', columns=['labelled_class'])
    label_noise['labels_used'] = 'noise'
    valid_labels = label_df[label_df.number_points > 10]

    predicted_classes = label_df.loc[:, [col for col in label_df.columns if 'dc_' in col]]
    prediction_features = label_df.loc[:, [col for col in label_df.columns if 'dc_' not in col]]

    # some have haralick data, some don't
    ldf_texture_segs = label_df.dropna().index
    ldf_texture_classes = predicted_classes.loc[ldf_texture_segs]
    ldf_texture_feats = prediction_features.loc[ldf_texture_segs]


def create_master_seg_table(root_fol):
    fol_list = [f for f in os.listdir(root_fol) if 'matlab' not in f]
    for i, fol in enumerate(fol_list):
        mp, mdf = separate_seg_data(os.path.join(root_fol, fol))
        if isinstance(mp, int):
            pass  # continue
        grpby = mp.groupby(['plot_id', 'sample_id'])
        for j, (name, grp) in enumerate(grpby):
            ft = create_feature_table_from_points(grp)
            ft = ft[ft.index.isin(mdf.loc[name].index.values)]
            ft = ft.reset_index().rename(columns=dict(supervox_label='seg_num'))
            ft.loc[:, 'plot_id'] = name[0]
            ft.loc[:, 'sample_id'] = name[1]
            ft = ft.set_index(['plot_id', 'sample_id', 'seg_num'])
            if not j:
                mft = ft
            else:
                mft = pd.concat([mft, ft])
        if not i:
            sft = mft
        else:
            sft = pd.concat([sft, mft])
        sft.to_hdf(
            '/mnt/spinny_store/cloudstor/e76243/Shared/Fuels3D end-user trials/Trial 1/segment_labelling/'
            'master_labels.h5', 'df', mode='w')
    return sft


def grab_all_spreadsheet_data(root_fol):
    fol_list = [f for f in os.listdir(root_fol) if np.logical_and(
        f != 'matlab', os.path.isdir(os.path.join(root_fol, f)))]
    for i, fol in enumerate(fol_list):
        mp, mdf = separate_seg_data(os.path.join(root_fol, fol))
        if isinstance(mp, int):
            continue
        if not i:
            df = mdf
        else:
            df = pd.concat([df, mdf])
    return df