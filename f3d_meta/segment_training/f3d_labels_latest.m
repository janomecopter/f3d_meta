% read in spreadsheet
[file, path] = uigetfile('*.xlsx', 'Please select xlsx containing training samples');
[sp, classes] = xlsread([path, file],1,'A2:B500');
start_ind = length(classes) + 1;
f = split(file, '.');
segname = f{1};

[file, path] = uigetfile('*.h5', 'Please select h5 containing point cloud info');
file = [path file];

blk1_labs = h5read(file, sprintf('/%s/block1_items', segname));
x_pos = find(startsWith(blk1_labs, 'x'));
y_pos = find(startsWith(blk1_labs, 'y'));
z_pos = find(startsWith(blk1_labs, 'z'));
data = h5read(file, sprintf('/%s/block1_values', segname));
x = data(x_pos, :);
y = data(y_pos, :);
z = data(z_pos, :);

blk4_labs = h5read(file, sprintf('/%s/block4_items', segname));
r_pos = find(startsWith(blk4_labs, 'r'));
g_pos = find(startsWith(blk4_labs, 'g'));
b_pos = find(startsWith(blk4_labs, 'b'));
int_pos = find(startsWith(blk4_labs, 'int'));
data = h5read(file, sprintf('/%s/block4_values', segname));
r = data(r_pos, :);
g = data(g_pos, :);
b = data(b_pos, :);
intensity = data(int_pos, :);

blk3_labs = h5read(file, sprintf('/%s/block3_items', segname));
seg_pos = find(startsWith(blk3_labs, 'supervox'));
data = h5read(file, sprintf('/%s/block3_values', segname));
segment_number = data(seg_pos, :)';

pc = pointCloud([x', y', z'], 'Color', [double(r')/2^16 double(g')/2^16 double(b')/2^16]);

% pc = lasdata('S9.las', 'loadall');
% segment_number = pc.intensity;
% pc = pointCloud([pc.x, pc.y, pc.z], 'Color', [double(pc.red)/65536, double(pc.green)/65536, double(pc.blue)/65536]);

close all;
f1 = figure;
a1 = subplot(1,1,1);
f2 = figure;
a2 = subplot(1,1,1);
Link = linkprop([a1, a2],{'CameraUpVector', 'CameraPosition',...
    'CameraTarget', 'XLim', 'YLim', 'ZLim'});
setappdata(f1, 'StoreTheLink', Link);
setappdata(f2, 'StoreTheLink', Link);
i = 0;

sp = sp(start_ind:end);
for segment = sp'
    % get the segment from the point cloud
    ind = segment_number == segment;
    disp(['Segment number: ', num2str(segment)])
    disp(['Number of Points: ', num2str(sum(ind))])
    if sum(ind) > 10
        pc_seg = pc.select(ind);
        [rotmat,cornerpoints,volume,surface,edgelength] = minboundbox(pc_seg.Location(:,1), ...
                                                                      pc_seg.Location(:,2), ...
                                                                      pc_seg.Location(:,3));
        axes(a1);
        pcshow(pc); hold on;
        plotminbox(cornerpoints, 'y');
        title(a1,segment);

        axes(a2);
        pcshow(pc); hold on;
        pc_seg = pointCloud(pc_seg.Location);
        colors = pcshow(pc_seg, 'MarkerSize', 14);                                                          
        %object_handles = findobj(a2)
    end
    if mod(i, 2) == 0
        input('Does Sam like soft cheese?')
    else
        input('Does Sam like hard cheese?')
    end
    i = i + 1;
    %delete(object_handles(2));
end
close all;




