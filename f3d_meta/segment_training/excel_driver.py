from openpyxl import Workbook, load_workbook
from openpyxl.worksheet.datavalidation import DataValidation
import numpy as np
import os
import pandas as pd
import SFP


def make_sheets(path, number_of_labels=40, class_list=None):
    """
    makes spreadsheets for digitisation of classes for samples in a plot
    :param path: path to pandas hdf5 file containing segmented point cloud information
    :param number_of_labels: number of labels to define using matlab
    :param class_list: custom list of classes (default list available if None selected)
    """
    if class_list is None:
        class_list = ['BareEarth', 'Rock', 'LeafLitter', 'WoodyDebris', 'Forb', 'Grass',
                      'Moss', 'Tree', 'ControlTarget', 'Noise', 'Mixed']
    plotname = '.'.join(os.path.split(path)[1].split('.')[:-1])
    pcs = SFP.read_hdf(path)

    for sample in pcs.keys():
        df = pcs[sample]
        segments = np.random.permutation(df.supervox_label.unique())[:number_of_labels]
        wb = Workbook()
        ws1 = wb.create_sheet("Classes")

        for c, value in enumerate(class_list):
            ws1.cell(row=c + 1, column=1).value = value

        ws2 = wb['Sheet']
        ws2.title = 'Segments'
        # ws2 = wb.create_sheet("Segments", 0)
        dv = DataValidation(type="list", formula1=f"=Classes!$A$1:$A${len(class_list)}", allow_blank=True)

        ws2.cell(row=1, column=1).value = 'Segment Number'
        ws2.cell(row=1, column=2).value = 'Class'
        ws2.add_data_validation(dv)
        for r, segment in enumerate(segments):
            ws2.cell(row=r + 2, column=1).value = segment
        dv.add(f'B2:B{number_of_labels + 1}')
        wb.save(os.path.join(os.path.split(path)[0], f'{plotname}_{sample}.xlsx'))
    print(f'xlsx files saved in {os.path.split(path)[0]}')


def reclass_sheets(fol_path):
    """
    helper function for adding extra classes - probably not required, left for completeness
    :param fol_path: folder containing spreadsheets
    """
    class_list = [
        'BareEarth', 'Rock', 'LeafLitter', 'WoodyDebris', 'Forb', 'Grass', 'Moss', 'Tree', 'ControlTarget',
        'Noise', 'Mixed']
    flist = [os.path.join(fol_path, f) for f in os.listdir(fol_path) if 'xlsx' in f]
    for path in flist:
        wb = load_workbook(path)
        wsc = wb['Classes']
        for c, value in enumerate(class_list):
            wsc.cell(row=c + 1, column=1).value = value
        wss = wb['Segments']
        dv = DataValidation(type="list", formula1=f"=Classes!$A$1:$A${len(class_list)}", allow_blank=True)
        wss.add_data_validation(dv)
        dv.add(f'B2:B{wss.max_row}')
        wb.save(path)


def load_class_info(xlsx_file):
    """
    grabs data from the validation spreadsheets and puts it in pd.Dataframe format for further consumption
    :param xlsx_file: path to xlsx file
    :return: pd.Dataframe containing spreadsheet info
    """
    wb = load_workbook(xlsx_file)
    ws = wb['Segments']
    seg_classes = dict()
    for item in ws[f'A2:B{ws.max_row}']:
        seg_classes.update({item[0].value: item[1].value.lower()})
    sc = pd.DataFrame.from_dict(seg_classes, orient='index', columns=['digitised_class'])
    return sc