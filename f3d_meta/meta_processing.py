import Metashape
import os


# construct marker list
marker_list = []
green_markers = ['G1', 'G2']
green_dirs = ['N', 'S', 'U']
red_markers = ['R1', 'R2']
red_dirs = ['E', 'W', 'U']
marker_arms = ['I', 'O']
green_list = ['{}{}{}'.format(a, b, c) for a in green_markers for b in green_dirs for c in marker_arms]
[marker_list.append(x)
 for x in ['{}{}{}'.format(a, b, c) for a in green_markers for b in green_dirs for c in marker_arms]]
[marker_list.append(x)
 for x in ['{}{}{}'.format(a, b, c) for a in red_markers for b in red_dirs for c in marker_arms]]
[marker_list.append(x)
 for x in ['UEU', 'UEL', 'UWU', 'UWL', 'USU', 'USL', 'UNU', 'UNL']]


def import_and_align(plot_path, savepath=None, cutoff_pc=0.7):
    """
    Python process to import and align all photos in a plot
    :param plot_path: folder containing folders corresponding to each sample
    :param savepath: location to save metashape document to
    :param cutoff_pc: percentage limit for disabling chunks based upon the number of photos aligned
    :return: good_samples, bad_samples: list
        returns lists containing sample identifiers based upon pass/fail of image alignment
    """
    # first, check plot file structure
    working_path = os.path.join(plot_path, 'images')
    # create a project
    app = Metashape.Application()
    app.cpu_enable = True
    doc = Metashape.Document()
    # get a list of the samples by folder
    sample_list = [d for d in os.listdir(working_path) if os.path.isdir(os.path.join(working_path, d))
                   and d[-5:] != 'files']
    # sort folders so the chunks correspond in numerical order
    sample_list.sort()
    # set savepath if none specified
    if savepath is None:
        savepath = os.path.join(working_path, '{}.psx'.format(working_path.split(os.path.sep)[-2]))
    # need some lists declared
    good_samples = list()
    bad_samples = list()
    # create a chunk per sample
    for sample_folder in sample_list:
        chunk = doc.addChunk()
        # set chunk ID
        chunk.label = sample_folder
        # addPhotos from sample_folder
        chunk.addPhotos([os.path.join(working_path, sample_folder, f)
                         for f in os.listdir(os.path.join(working_path, sample_folder))
                         if '.JPG' in f])
        # photo matching and alignment (not sure optimize is required, doing it anyway)
        chunk.matchPhotos(
            downscale=1,
            generic_preselection=True,
            reference_preselection=False)
        chunk.alignCameras()
        try:
            chunk.optimizeCameras()
        except Exception:  # would like to make this neater, but I don't remember the exact error it throws here
            pass
        # let's count some cameras
        num_cameras = len(chunk.cameras)
        num_aligned = 0
        # check if camera has a transformation associated, if so it's been matched
        for camera in chunk.cameras:
            if camera.transform:
                num_aligned += 1
        # kill process if less than cutoff_pc photos aligned
        if num_aligned / num_cameras < cutoff_pc:
            # chunk.enabled = False
            doc.remove([chunk])
            doc.save(savepath)
            bad_samples.append(sample_folder)
            continue
        # create markers
        for i, marker_id in enumerate(marker_list):
            marker = chunk.addMarker()
            marker.label = marker_id
            # add scalebars
            if i % 2:
                sc = chunk.addScalebar(*chunk.markers[-2:])
                sc.reference.distance = 0.2
        chunk.scalebars[-2].reference.enabled = False
        # add reference data
        marker_labels = [marker.label for marker in chunk.markers]
        marker_ref_locs = {'USL': (100., 500., 30.),
                           'USU': (100., 500., 30.2),
                           'UNL': (100., 500.022, 30.)}
        for k, v in marker_ref_locs.items():
            mk = chunk.markers[marker_labels.index(k)]
            mk.reference.location = Metashape.Vector(v)
        # save file
        doc.save(savepath)
        # append sample_id to good list
        good_samples.append(sample_folder)
    return good_samples, bad_samples


def build_clouds(metashape_file):
    """
    Process to build dense point clouds in projects with control information
    :param metashape_file: path to Metashape file
    :return status: nominally zero
    """
    # specify output folder
    out_folder = os.path.join(os.path.dirname(os.path.split(metashape_file)[0]), 'ms_output')
    # creates a Application instance in order to open the project
    app = Metashape.Application()
    # turns on CPU help when building point clouds
    app.cpu_enable = True
    # open the Metashape project
    doc = Metashape.Document()
    doc.open(metashape_file)
    # for each chunk, build clouds
    for i, chunk in enumerate(doc.chunks):
        if chunk.enabled:
            chunk.buildDepthMaps()
            chunk.buildDenseCloud()
            # export the dense clouds to a LAS format file in the 'ms_output' folder
            chunk.exportPoints(os.path.join(
                out_folder, os.path.split(metashape_file)[1][:-4] + '_{}.las'.format(chunk.label)),
                format=Metashape.PointsFormatLAS)
        # save project
        doc.save(metashape_file)
    return 0

