# f3d_meta: handles and processes for producing metrics for Fuels3D point clouds

[![Python](https://img.shields.io/badge/python-3.6%20%7C%203.7-blue)](https://www.python.org/)

## What is it?

**f3d_meta** is a Python package which provides the workflow for the production of fuel related metrics derived from 
point clouds. This package gives the user the ability to drive images from a structured image acquisition process 
through a number of steps, including processing of images using Agisoft Metashape, building of point clouds, and 
segmentation and classification processes using open-source tools.

## Where to get it?

The source code for the project is hosted at:
https://gitlab.com/janomecopter/f3d_meta

## Dependencies
- [NumPy](https://www.numpy.org)
- [Pandas](https://github.com/pandas-dev/pandas)
- [scikit-image](https://scikit-image.org/)
- [scikit-learn](https://scikit-learn.org/)
- [openpyxl](https://pypi.org/project/openpyxl/)
- [mahotas](https://pypi.org/project/mahotas/)
- [Metashape](https://www.agisoft.com/downloads/installer/)
- [SFP](https://gitlab.com/janomecopter/stupid-forest-points)

## Installation from sources
To install f3d_meta from source, you will need Cython in addition to the dependencies listed above. Cython can be 
installed from pypi:

```sh
pip install cython
```

In the `f3d_meta` directory (same one where you found this file after cloning the git repo), execute:

```sh
python setup.py install
```

Installation requires the use of the SFP library, which is a point cloud processing library used for handling and 
processing large point clouds - this has a separate set of installation requirements which can be found on the project's
GitLab site. Use of the library also requires a valid license of the Agisoft Metashape software active on the system. 
In order for this license to be detected properly, the license should be activated using the program's GUI interface, 
and environment variables should be set as such to ensure the license is detected:

```sh
export agisoft_LICENSE=/path/to/root/folder/of/metashape/install
```

Validity of the Metashape license can be checked by running the following commands in a Python console:

```python
import Metashape
Metashape.License().valid
```

## License
[GNU GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)
